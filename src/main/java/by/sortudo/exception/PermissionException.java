package by.sortudo.exception;

/**
 * Class of exception which can happen if the client doesn't have access rights.
 *
 * @author Elena Alekhnovich
 */
public class PermissionException extends Exception {

    public PermissionException(){
        super();
    }

    public PermissionException(String message){
        super(message);
    }
}
