package by.sortudo.exception;

/**
 * Class of exception which can happen in case of invalid parameters.
 *
 * @author Elena Alekhnovich
 */
public class InvalidParameterException extends Exception {

    public InvalidParameterException(){
        super();
    }

    public InvalidParameterException(String message){
        super(message);
    }
}
