package by.sortudo.exception;

/**
 * Class of exception which can happen during making authorization.
 *
 * @author Elena Alekhnovich
 */
public class AuthorizationException extends Exception {

    public AuthorizationException(){
        super();
    }

    public AuthorizationException(String message){
        super(message);
    }
}
