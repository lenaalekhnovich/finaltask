package by.sortudo.tests;

import by.sortudo.bean.AudioTrack;
import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.AudioTrackDAO;
import by.sortudo.logic.command.AudioTrackCommand;
import by.sortudo.servlet.RequestContent;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

/**
 * Created by Босс on 26.05.2017.
 */
public class AudioTrackCommandTest {

    private static AudioTrackCommand audioTrackCommand;
    private static RequestContent content;
    private static AudioTrackDAO dao;

    @BeforeClass
    public static void init() {
        audioTrackCommand = new AudioTrackCommand();
        content = new RequestContent();
        dao = AudioTrackDAO.getInstance();
        content.setParameter("name", "test");
        content.setParameter("author", "test");
        content.setParameter("genre", "test");
        content.setParameter("year", "2017");
        content.setParameter("price", "1.0");
        content.setSessionAttribute("audioId", "38");
        content.setSessionAttribute("clientType", "ADMIN");
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeEmptyCommandTest() {
        content.setParameter("command", "none");
        String page = audioTrackCommand.execute(content);
        Assert.assertNull(page);
    }

    @Test
    public void addAudioTrackTest() {
        String page = audioTrackCommand.addAudio(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.audioTracks"));
    }

    @Test
    public void updateAudioTrackTest() {
        String page = audioTrackCommand.updateAudio(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.audioInfo"));
    }

    @Test
    public void deleteAudioTrackTest() {
        content.setParameter("audioId", "23");
        String page = audioTrackCommand.deleteAudio(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.audioTracks"));
    }

    @Test
    public void getAudioTrackTest() {
        AudioTrack audioTrack = dao.getEntityById(1);
        Assert.assertNotNull(audioTrack);
    }

    @Test
    public void findAllAudioTracksTest() {
        List<AudioTrack> list = dao.findAll("");
        Assert.assertNotNull(list);
    }

    @Test
    public void findRecommendsTest() {
        List<AudioTrack> list = dao.findRecommendsForClient(1, "");
        Assert.assertNotNull(list);
    }
}
