package by.sortudo.tests;

import by.sortudo.bean.AudioTrack;
import by.sortudo.bean.Bonus;
import by.sortudo.bean.Client;
import by.sortudo.logic.validation.AudioTrackValidation;
import by.sortudo.logic.validation.BonusValidation;
import by.sortudo.logic.validation.ClientValidation;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Босс on 25.05.2017.
 */
public class ValidationTest {

    @Test
    public void validateAudioTrackTest() {
        AudioTrack audioTrack = new AudioTrack();
        audioTrack.setAuthor("AaВв-& a");
        audioTrack.setName("Abc-аб в");
        audioTrack.setYear(1998);
        audioTrack.setGenre("Abc -аб");
        Assert.assertTrue(AudioTrackValidation.isValid(audioTrack));
    }

    @Test
    public void validateWrongAudioTrackTest() {
        AudioTrack audioTrack = new AudioTrack();
        audioTrack.setAuthor("select * from client;");
        audioTrack.setName("drop audio_track");
        audioTrack.setYear(1499);
        audioTrack.setGenre("Abc123-аб");
        Assert.assertFalse(AudioTrackValidation.isValid(audioTrack));
    }

    @Test
    public void validateBonusTest() {
        Bonus bonus = new Bonus();
        bonus.setName("Free audio");
        bonus.setSize(12);
        bonus.setUnits("шт");
        Assert.assertTrue(BonusValidation.isValid(bonus));
    }

    @Test
    public void validateWrongBonusTest() {
        Bonus bonus = new Bonus();
        bonus.setName("drop bonus");
        bonus.setSize(12);
        bonus.setUnits("123");
        Assert.assertFalse(BonusValidation.isValid(bonus));
    }

    @Test
    public void validateClientTest() {
        Client client = new Client();
        client.setName("Ariel");
        client.setLogin("asAd1-_");
        client.setEmail("asSвВd1-_@google.com");
        client.setCreditCard("11111111111");
        Assert.assertTrue(ClientValidation.isValid(client));
    }

    @Test
    public void validateWrongClientTest() {
        Client client = new Client();
        client.setName("select * from order");
        client.setLogin("update client set client_name=asd");
        client.setEmail("asSвВd1google.com");
        client.setCreditCard("1");
        Assert.assertFalse(ClientValidation.isValid(client));
    }


}
