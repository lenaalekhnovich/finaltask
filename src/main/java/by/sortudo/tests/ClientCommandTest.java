package by.sortudo.tests;

import by.sortudo.bean.Client;
import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.ClientDAO;
import by.sortudo.logic.command.ClientCommand;
import by.sortudo.servlet.RequestContent;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

/**
 * Created by Босс on 26.05.2017.
 */
public class ClientCommandTest {
    private static ClientCommand clientCommand;
    private static RequestContent content;
    private static ClientDAO dao;

    @BeforeClass
    public static void init() {
        clientCommand = new ClientCommand();
        content = new RequestContent();
        dao = ClientDAO.getInstance();
        content.setParameter("name", "Test");
        content.setParameter("login", "test");
        content.setParameter("dateBirth", "2000-01-01");
        content.setParameter("email", "aaa@gmail.com");
        content.setParameter("password", "1234");
        content.setParameter("creditCard", "111111111111");
    }

    @Before
    public void addClientTest() {
        if (dao.getClientByLogin(content.getParameter("login")) == null) {
            clientCommand.addClient(content);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeEmptyCommandTest() {
        content.setParameter("command", "none");
        String page = clientCommand.execute(content);
        Assert.assertNull(page);
    }

    @Test
    public void logInByUserTest() {
        String page = clientCommand.userLogIn(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.mainUser"));
    }

    @Test
    public void updateClientTest() {
        Client client = dao.getClientByLogin("test");
        content.setSessionAttribute("clientType", "USER");
        content.setSessionAttribute("clientId", Integer.valueOf(client.getId()).toString());
        content.setParameter("name", "Testu");
        String page = clientCommand.updateClient(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.mainUser"));
    }

    @Test
    public void updateClientByAdminTest() {
        content.setSessionAttribute("clientType", "ADMIN");
        String page = clientCommand.updateClient(content);
        Assert.assertNull(page);
    }

    @Test
    public void updateBonusTest() {
        content.setSessionAttribute("clientType", "ADMIN");
        content.setParameter("bonusId", "1");
        String page = clientCommand.updateBonus(content);
        Assert.assertNotNull(page);
    }

    @Test
    public void findAllClientsTest() {
        content.setSessionAttribute("clientType", "ADMIN");
        content.setSessionAttribute("clientId", "ADMIN");
        List<Client> list = dao.findAll("");
        Assert.assertNotNull(list);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void deleteClientTest() {
        boolean result = dao.delete(new Client());
        Assert.assertFalse(result);
    }


}
