package by.sortudo.tests;

import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.OrderDAO;
import by.sortudo.logic.command.OrderCommand;
import by.sortudo.servlet.RequestContent;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by Босс on 26.05.2017.
 */
public class OrderCommandTest {
    private static OrderCommand orderCommand;
    private static RequestContent content;
    private static OrderDAO dao;

    @BeforeClass
    public static void init() {
        orderCommand = new OrderCommand();
        content = new RequestContent();
        dao = OrderDAO.getInstance();
        content.setParameter("audioId", "38");
        content.setSessionAttribute("clientId", "22");
        content.setSessionAttribute("clientType", "USER");
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeEmptyCommandTest() {
        content.setParameter("command", "none");
        String page = orderCommand.execute(content);
        Assert.assertNull(page);
    }

    @Test
    public void addOrderTest() {
        String page = orderCommand.addOrder(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.audioTracks"));
    }

    @Test
    public void updateOrderTest() {
        String page = orderCommand.updateOrder(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.mainUser"));
    }

    @Test
    public void deleteOrderTest() {
        String page = orderCommand.deleteOrder(content);
        Assert.assertEquals(page, PageConfiguration.getProperty("path.page.basket"));
    }
}
