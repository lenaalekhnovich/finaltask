package by.sortudo.logic;

import by.sortudo.logic.command.*;

/**
 * Enum class where type of the command is defined and depending on this type
 * corresponding object of {@link ActionCommand} is created.
 *
 * @author Elena Alekhnovich
 */
public enum TypeCommandEnum {
    AUDIOTRACK(new AudioTrackCommand()),
    CLIENT (new ClientCommand()),
    BONUS (new BonusCommand()),
    ORDER (new OrderCommand()),
    REVIEW (new ReviewCommand()),
    LOCALE (new LocaleCommand());

    ActionCommand typeCommand;

    TypeCommandEnum(ActionCommand typeCommand){
        this.typeCommand = typeCommand;
    }

    public ActionCommand getCurrentTypeCommand() {
        return typeCommand;
    }

}
