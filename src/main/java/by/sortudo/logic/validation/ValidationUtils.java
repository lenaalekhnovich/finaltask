package by.sortudo.logic.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is the top of hierarchy of validation's classes. It consists method
 * for basic validation.
 *
 * @author Elena Alekhnovich
 */
public class ValidationUtils {
    /**
     * This method checks string for its validation. Method protects code
     * from sql injections that's why string shouldn't contain words
     * like select, update, insert, etc.
     *
     * @param parameter checking for validation string
     * @return {@code true} if string is valid.
     */
    public static boolean isValid(String parameter) {
        String regex = "(?i)select|update|insert|delete|drop|alert";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(parameter);
        return (!matcher.find());
    }
}
