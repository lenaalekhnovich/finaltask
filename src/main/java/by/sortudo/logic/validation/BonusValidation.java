package by.sortudo.logic.validation;

import by.sortudo.bean.Bonus;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class consists all methods that are required for bonus validation.
 *
 * @author Elena Alekhnovich
 * @see ValidationUtils
 */
public class BonusValidation extends ValidationUtils {
    /**
     * This method checks object of {@link Bonus} for validation.
     *
     * @param bonus checking for validation bonus.
     * @return {@code true} if bonus is valid.
     */
    public static boolean isValid(Bonus bonus) {
        return checkSize(bonus.getSize()) && checkUnits(bonus.getUnits()) && checkName(bonus.getName());
    }

    /**
     * This method checks size of bonus for validation.
     *
     * @param size checking for validation size of bonus.
     * @return {@code true} if size is valid.
     */
    private static boolean checkSize(int size) {
        return (size > 0 && size < 101);
    }

    /**
     * This method checks units of bonus for validation.
     *
     * @param units checking for validation units of bonus.
     * @return {@code true} if units are valid.
     */
    private static boolean checkUnits(String units) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[а-яa-z%]+$");
        Matcher matcher = pattern.matcher(units);
        if (!matcher.matches() || !isValid(units)) {
            check = false;
        }
        return check;
    }

    /**
     * This method checks name of bonus for validation.
     *
     * @param name checking for validation name of bonus.
     * @return {@code true} if name is valid.
     */
    private static boolean checkName(String name) {
        return (StringUtils.isNotEmpty(name) && isValid(name));
    }
}
