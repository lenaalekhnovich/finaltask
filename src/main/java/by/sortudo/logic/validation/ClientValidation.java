package by.sortudo.logic.validation;

import by.sortudo.bean.Client;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class consists all methods that are required for client's validation.
 *
 * @author Elena Alekhnovich
 * @see ValidationUtils
 */
public class ClientValidation extends ValidationUtils {
    /**
     * This method checks object of {@link Client} for validation.
     *
     * @param client checking for validation client.
     * @return {@code true} if client is valid.
     */
    public static boolean isValid(Client client) {
        return checkName(client.getName()) && checkLogin(client.getLogin())
                && checkEmail(client.getEmail()) && checkCredit(client.getCreditCard());
    }

    /**
     * This method checks name of client for validation.
     *
     * @param name checking for validation name of client.
     * @return {@code true} if name is valid.
     */
    private static boolean checkName(String name) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[А-ЯA-Z][а-яa-z]+$");
        Matcher matcher = pattern.matcher(name);
        if (!matcher.matches() || !isValid(name)) {
            check = false;
        }
        return check;
    }

    /**
     * This method checks login of client for validation.
     *
     * @param login checking for validation login of client.
     * @return {@code true} if login is valid.
     */
    private static boolean checkLogin(String login) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[\\w_-]+$");
        Matcher matcher = pattern.matcher(login);
        if (!matcher.matches() || !isValid(login)) {
            check = false;
        }
        return check;
    }

    /**
     * This method checks email of client for validation.
     *
     * @param email checking for validation email of client.
     * @return {@code true} if email is valid.
     */
    private static boolean checkEmail(String email) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[\\wа-яА-Я-.]+@[a-zа-я]{2,6}\\.[a-zа-я]{2,3}$");
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches() || !isValid(email)) {
            check = false;
        }
        return check;
    }

    /**
     * This method checks credit card of client for validation.
     *
     * @param creditCard checking for validation credit card of client.
     * @return {@code true} if credit card is valid.
     */
    private static boolean checkCredit(String creditCard) {
        return (StringUtils.isNumeric(creditCard) && creditCard.length() > 9 && isValid(creditCard));
    }
}
