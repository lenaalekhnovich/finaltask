package by.sortudo.logic.validation;

import by.sortudo.bean.AudioTrack;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class consists all methods that are required for audio track's validation.
 *
 * @author Elena Alekhnovich
 * @see ValidationUtils
 */
public class AudioTrackValidation extends ValidationUtils {

    /**
     * This method checks object of {@link AudioTrack} for validation.
     *
     * @param audioTrack checking for validation audio track.
     * @return {@code true} if audio track is valid.
     */
    public static boolean isValid(AudioTrack audioTrack) {
        return (checkAuthor(audioTrack.getAuthor()) && checkName(audioTrack.getName())
                && checkGenre(audioTrack.getGenre()) && checkYear(audioTrack.getYear()));
    }

    /**
     * This method checks author of audio track for validation.
     *
     * @param author checking for validation author of audio track.
     * @return {@code true} if author is valid.
     */
    private static boolean checkAuthor(String author) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[\\wа-яА-Я\\s&-]+$");
        Matcher matcher = pattern.matcher(author);
        if (!matcher.matches() || !isValid(author)) {
            check = false;
        }
        return check;
    }

    /**
     * This method checks name of audio track for validation.
     *
     * @param name checking for validation name of audio track.
     * @return {@code true} if name is valid.
     */
    private static boolean checkName(String name) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[\\wа-яА-Я\\s-]+$");
        Matcher matcher = pattern.matcher(name);
        if (!matcher.matches() || !isValid(name)) {
            check = false;
        }
        return check;
    }

    /**
     * This method checks year of audio track for validation.
     *
     * @param year checking for validation year of audio track.
     * @return {@code true} if year is valid.
     */
    private static boolean checkYear(int year) {
        return (year > 1500 && year < 2018);
    }

    /**
     * This method checks genre of audio track for validation.
     *
     * @param genre checking for validation genre of audio track.
     * @return {@code true} if genre is valid.
     */
    private static boolean checkGenre(String genre) {
        boolean check = true;
        Pattern pattern = Pattern.compile("^[\\wа-яА-Я-\\s]+$");
        Matcher matcher = pattern.matcher(genre);
        if (!matcher.matches()) {
            check = false;
        }
        return check;
    }

}
