package by.sortudo.logic;

import by.sortudo.logic.command.ActionCommand;
import by.sortudo.logic.command.EmptyCommand;
import by.sortudo.servlet.RequestContent;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is a factory of creating corresponding command. It includes only one method
 * to define command.
 *
 * @author Elena Alekhnovich
 */
public class ActionFactory {
    /**
     * Object for writing logs to a file in case of exceptions.
     */
    Logger logger = LogManager.getLogger("by.sortudo.logic");

    /**
     * This method is used to define which command factory should create. To find name of the
     * required command this method uses enum {@link TypeCommandEnum}.
     *
     * @param request content of the request.
     * @return required command; in case of undefined command - empty command.
     * @see ActionCommand
     */
    public ActionCommand defineCommand(RequestContent request){
        ActionCommand currentCommand = new EmptyCommand();
        String actionObject = request.getParameter("commandObject");
        if(StringUtils.isEmpty(actionObject)){
            return currentCommand;
        }
        try{
            TypeCommandEnum currentEnum = TypeCommandEnum.valueOf(actionObject.toUpperCase());
            currentCommand = currentEnum.getCurrentTypeCommand();
        } catch (IllegalArgumentException e){
            logger.error("negative argument: " + e);
        }
        return currentCommand;
    }
}
