package by.sortudo.logic.command;

import by.sortudo.bean.AudioTrack;
import by.sortudo.bean.Review;
import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.AudioTrackDAO;
import by.sortudo.database.ReviewDAO;
import by.sortudo.exception.InvalidParameterException;
import by.sortudo.exception.PermissionException;
import by.sortudo.filter.ClientTypeEnum;
import by.sortudo.logic.CommandEnum;
import by.sortudo.logic.validation.AudioTrackValidation;
import by.sortudo.logic.validation.ValidationUtils;
import by.sortudo.servlet.RequestContent;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * This class is implementation of interface {@link ActionCommand}. It describes all commands
 * which are relevant to audio track and received from the client such as add, update,
 * find all, etc. To get information and to add new about audio track this class consists
 * object {@link AudioTrackDAO}.
 *
 * @author Elena Alekhnovich
 */
public class AudioTrackCommand implements ActionCommand {
    /**
     * Object of DAO class for working with database's entity audio track.
     */
    private AudioTrackDAO dbConnection;

    /**
     * Default constructor where object of {@link AudioTrackDAO} is created.
     */
    public AudioTrackCommand() {
        dbConnection = AudioTrackDAO.getInstance();
    }

    /**
     * Method to define command from client and to execute corresponding to this command action.
     * Depending on the received command this method returns required page.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        switch (CommandEnum.valueOf(action.toUpperCase())) {
            case ADD:
                page = addAudio(request);
                break;
            case UPDATE:
                page = updateAudio(request);
                break;
            case DELETE:
                page = deleteAudio(request);
                break;
            case FIND:
                page = getPage(request);
                break;
            case GET:
                page = getAudioPage(request);
                break;
            case RECOMMENDS:
                page = findRecommends(request);
                break;
        }
        return page;
    }

    /**
     * Method for creating new audio track.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String addAudio(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.USER)) {
            return page;
        }
        try {
            AudioTrack audiotrack = new AudioTrack();
            audiotrack = setAudioTrack(request, audiotrack);
            if (!AudioTrackValidation.isValid(audiotrack)) {
                throw new InvalidParameterException("Parameters of audio track're invalid");
            }
            dbConnection.create(audiotrack);
            page = PageConfiguration.getProperty("path.get.audioTracks");
        } catch (InvalidParameterException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method for updating audio track.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String updateAudio(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.USER)) {
            return page;
        }
        try {
            int id = Integer.parseInt(request.getSessionAttribute("audioId").toString());
            AudioTrack audiotrack = new AudioTrack();
            audiotrack.setId(id);
            audiotrack = setAudioTrack(request, audiotrack);
            if (!AudioTrackValidation.isValid(audiotrack)) {
                throw new InvalidParameterException("Parameters of audio track're invalid");
            }
            dbConnection.update(audiotrack);
            page = PageConfiguration.getProperty("path.get.audioInfo") + id;
        } catch (InvalidParameterException | NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * This method is for defining required page with audio tracks.
     * Depending on the type of client(admin, user) this method returns corresponding page.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String getPage(RequestContent request) {
        List<AudioTrack> list = null;
        String page;
        String clientType = (String) request.getSessionAttribute("clientType");
        String strFind = request.getParameter("audioFind");
        if (StringUtils.isEmpty(strFind)) {
            strFind = "";
        }
        try {
            if (!ValidationUtils.isValid(strFind)) {
                throw new InvalidParameterException("Parameter " + strFind + " is invalid");
            }
            switch (ClientTypeEnum.valueOf(clientType.toUpperCase())) {
                case USER:
                    int id = Integer.parseInt((String) request.getSessionAttribute("clientId"));
                    list = dbConnection.findAudioForClient(id, strFind);
                    break;
                case ADMIN:
                    list = dbConnection.findAll(strFind);
                    break;
            }
            ActionCommand.setPages(request, list.size(), 6);
            request.setAttribute("audioFind", strFind);
            request.setAttribute("audiotracks", list);
            page = PageConfiguration.getProperty("path.page.audioTracks");
        } catch (InvalidParameterException | NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method for getting page of the audio track depending on its id.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String getAudioPage(RequestContent request) {
        String page;
        int id;
        try {
            if(request.getParameter("audioId") != null) {
                id = Integer.parseInt(request.getParameter("audioId"));
                request.setSessionAttribute("audioId", id);
            }
            id = Integer.parseInt(request.getSessionAttribute("audioId").toString());
            AudioTrack audioTrack = dbConnection.getEntityById(id);
            List<Review> list = ReviewDAO.getInstance().findAll(id);
            audioTrack.setReviewList(list);
            ActionCommand.setPages(request, list.size(), 3);
            request.setAttribute("audiotrack", audioTrack);
            page = PageConfiguration.getProperty("path.page.audioInfo");
        } catch (NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method is designed to searching for audio tracks that are recommended for this client.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String findRecommends(RequestContent request) {
        List<AudioTrack> list;
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        String strFind = request.getParameter("audioFind");
        if (StringUtils.isEmpty(strFind)) {
            strFind = "";
        }
        if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.ADMIN)) {
            return page;
        }
        try {
            if (!ValidationUtils.isValid(strFind)) {
                throw new InvalidParameterException("Parameter " + strFind + " is invalid");
            }
            int id = Integer.parseInt((String) request.getSessionAttribute("clientId"));
            list = dbConnection.findRecommendsForClient(id, strFind);
            page = PageConfiguration.getProperty("path.page.recommends");
            ActionCommand.setPages(request, list.size(), 6);
            request.setAttribute("audioFind", strFind);
            request.setAttribute("audiotracks", list);
        } catch (InvalidParameterException | NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method for deleting corresponding audio track by its id.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String deleteAudio(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        try {
            if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.USER)) {
                throw new PermissionException("No permissions for this action");
            }
            int id = Integer.parseInt(request.getParameter("audioId"));
            String pageNumber = "&pageButton=" + (request.getParameter("page") == null ? 1 :
                    request.getParameter("page"));
            String audioFind = request.getParameter("audioFind");
            AudioTrack audioTrack = new AudioTrack();
            audioTrack.setId(id);
            dbConnection.delete(audioTrack);
            page = PageConfiguration.getProperty("path.get.audioTracks") + audioFind + pageNumber;
        } catch (NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        } catch (PermissionException e) {
            logger.error("negative argument: " + e);
        }
        return page;
    }

    /**
     * Method for getting data from request's content and updating {@link AudioTrack} object.
     *
     * @param request    content of the request.
     * @param audiotrack created audio track's object.
     * @return updated audio track's object.
     */
    private AudioTrack setAudioTrack(RequestContent request, AudioTrack audiotrack) {
        audiotrack.setName(request.getParameter("name"));
        audiotrack.setAuthor(request.getParameter("author"));
        audiotrack.setYear(Integer.parseInt(request.getParameter("year")));
        audiotrack.setGenre(request.getParameter("genre"));
        audiotrack.setPrice(Float.parseFloat(request.getParameter("price")));
        if (request.getParameter("site") != null) {
            audiotrack.setSite(request.getParameter("site"));
        }
        return audiotrack;
    }
}
