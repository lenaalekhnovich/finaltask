package by.sortudo.logic.command;

import by.sortudo.servlet.RequestContent;

/**
 * This class is implementation of interface {@link ActionCommand}.
 * It describes command which execute if undefined command was received from client.
 *
 * @author Elena Alekhnovich
 */
public class EmptyCommand implements ActionCommand {
    /**
     * Method where depending on the client's type returns required page.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        ClientCommand clientCommand = new ClientCommand();
        return clientCommand.getClientPage(request);
    }
}
