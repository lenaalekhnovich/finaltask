package by.sortudo.logic.command;

import by.sortudo.servlet.RequestContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This interface describes command that received from client.
 * It defines action {@link ActionCommand#execute(RequestContent)} that should be implement.
 *
 * @author Elena Alekhnovich
 */
public interface ActionCommand {

    /**
     * Object for writing logs to a file in case of exceptions.
     */
    Logger logger = LogManager.getLogger("by.sortudo.logic");

    /**
     * Method to set pagination.
     *
     * @param request  content of the request.
     * @param listSize amount of objects on the page.
     * @param size     required amount of pages.
     */
    static void setPages(RequestContent request, int listSize, int size) {
        int pageNumber = request.getParameter("pageButton") == null ? 1 :
                Integer.parseInt(request.getParameter("pageButton"));
        int pageSize = listSize % size == 0 ? listSize / size : listSize / size + 1;
        if(pageNumber > pageSize){
            pageNumber = pageSize;
        }
        request.setAttribute("page", pageNumber);
        request.setAttribute("pageSize", pageSize);
    }

    /**
     * Method to execute some action.
     *
     * @param request content of the request.
     * @return required page.
     */
    String execute(RequestContent request);

}
