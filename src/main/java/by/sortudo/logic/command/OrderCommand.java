package by.sortudo.logic.command;

import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.ClientDAO;
import by.sortudo.database.OrderDAO;
import by.sortudo.exception.PermissionException;
import by.sortudo.filter.ClientTypeEnum;
import by.sortudo.logic.CommandEnum;
import by.sortudo.bean.AudioTrack;
import by.sortudo.bean.Client;
import by.sortudo.bean.Order;
import by.sortudo.servlet.RequestContent;

import java.util.LinkedList;
import java.util.List;

/**
 * This class is implementation of interface {@link ActionCommand}. It describes all commands
 * which are relevant to order and received from the client such as add, update,
 * find all, etc. To get information and to add new about order this class consists
 * object {@link OrderDAO}.
 *
 * @author Elena Alekhnovich
 */
public class OrderCommand implements ActionCommand {
    /**
     * Object of DAO class for working with database's entity order.
     */
    private OrderDAO dbConnection;

    /**
     * Default constructor where object of {@link OrderDAO} is created.
     */
    public OrderCommand() {
        dbConnection = OrderDAO.getInstance();
    }

    /**
     * Method to define command from client and to execute corresponding to this command action.
     * Depending on the received command this method returns required page.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        try {
            if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.ADMIN)) {
                throw new PermissionException("No permissions for this action");
            }
            String action = request.getParameter("command");
            switch (CommandEnum.valueOf(action.toUpperCase())) {
                case ADD:
                    page = addOrder(request);
                    break;
                case ADDRECOMMENDS:
                    page = addFromRecommends(request);
                    break;
                case UPDATE:
                    page = updateOrder(request);
                    break;
                case UPDATEBONUS:
                    page = updateWithBonus(request);
                    break;
                case DELETE:
                    page = deleteOrder(request);
                    break;
                case FIND:
                    page = getOrderPage(request);
                    break;

            }
        } catch (PermissionException e) {
            logger.error("negative argument: " + e);
        }
        return page;
    }

    /**
     * This method is for getting required page with orders.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String getOrderPage(RequestContent request) {
        ClientDAO clientDb = ClientDAO.getInstance();
        List<AudioTrack> listAudio = new LinkedList<>();
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        List<Order> listOrders = dbConnection.getOrdersByClientId(clientId, "В корзине");
        for (Order listOrder : listOrders) {
            listAudio.add(listOrder.getAudioTrack());
        }
        request.setAttribute("audiotracks", listAudio);
        Client client = clientDb.getEntityById(clientId);
        request.setAttribute("client", client);
        return PageConfiguration.getProperty("path.page.basket");
    }

    /**
     * Method for adding new order for client.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String addOrder(RequestContent request) {
        AudioTrackCommand audioTrackCommand = new AudioTrackCommand();
        String page;
        try {
            int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
            int audioId = Integer.parseInt(request.getParameter("audioId"));
            String pageNumber = "&pageButton=" + (request.getParameter("page") == null ? 1 :
                    request.getParameter("page"));
            String audioFind = request.getParameter("audioFind");
            AudioTrack audioTrack = new AudioTrack();
            audioTrack.setId(audioId);
            Order order = new Order();
            order.setClientId(clientId);
            order.setAudioTrack(audioTrack);
            order.setState("В корзине");
            dbConnection.create(order);
            page = PageConfiguration.getProperty("path.get.audioTracks") + audioFind + pageNumber;
        } catch (NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method for adding new order for client from recommendations.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String addFromRecommends(RequestContent request) {
        String strFind = request.getParameter("audioFind");
        String pageNumber = "&pageButton=" + (request.getParameter("page") == null ? 1 :
                request.getParameter("page"));
        return (addOrder(request) == null ? null : PageConfiguration.getProperty("path.get.recommends")
                + strFind + pageNumber);
    }

    /**
     * Method for updating order.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String updateOrder(RequestContent request) {
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        Order order = new Order();
        order.setClientId(clientId);
        dbConnection.update(order);
        return PageConfiguration.getProperty("path.get.main");
    }

    /**
     * Method for updating order using bonus.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String updateWithBonus(RequestContent request) {
        ClientDAO clientDb = ClientDAO.getInstance();
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        Client client = clientDb.getEntityById(clientId);
        client.setBonus(null);
        clientDb.update(client);
        return updateOrder(request);
    }

    /**
     * Method for deleting corresponding order.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String deleteOrder(RequestContent request) {
        String page;
        try {
            int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
            int audioId = Integer.parseInt(request.getParameter("audioId"));
            Order order = new Order();
            order.setClientId(clientId);
            AudioTrack audio = new AudioTrack();
            audio.setId(audioId);
            order.setAudioTrack(audio);
            dbConnection.delete(order);
            page = PageConfiguration.getProperty("path.get.basket");
        } catch (NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method to get all client's orders.
     *
     * @param request content of the request.
     * @return list of the orders.
     */
    public List<Order> getClientsAudio(RequestContent request) {
        List<Order> listOrder;
        int clientId = Integer.parseInt(request.getSessionAttribute("clientId").toString());
        listOrder = dbConnection.getOrdersByClientId(clientId, "Оплачен");
        return listOrder;
    }


}
