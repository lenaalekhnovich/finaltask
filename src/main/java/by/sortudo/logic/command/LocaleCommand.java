package by.sortudo.logic.command;

import by.sortudo.configuration.PageConfiguration;
import by.sortudo.logic.LocaleEnum;
import org.apache.commons.lang3.StringUtils;
import by.sortudo.servlet.RequestContent;

/**
 * This class is implementation of interface {@link ActionCommand}.
 * It realises changing language on the page.
 *
 * @author Elena Alekhnovich
 */
public class LocaleCommand implements ActionCommand {
    /**
     * Method to get required language from content of the page and set it.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        String lang = request.getParameter("lang");
        LocaleEnum langName = LocaleEnum.valueOf(lang.toUpperCase());
        String value = langName.getLocale();
        request.setSessionAttribute("locale", value);
        return getPage(request);
    }

    /**
     * Method to get page in case of client's type.
     *
     * @param request content of the request.
     * @return required page.
     */
    private String getPage(RequestContent request) {
        ClientCommand clientCommand = new ClientCommand();
        String page = PageConfiguration.getProperty("path.page.index");
        String clientType = request.getCookie("clientType");
        if (StringUtils.isNotEmpty(clientType)) {
            return clientCommand.getClientPage(request);
        }
        return page;
    }
}
