package by.sortudo.logic.command;

import by.sortudo.bean.Bonus;
import by.sortudo.bean.Client;
import by.sortudo.configuration.AdminConfiguration;
import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.ClientDAO;
import by.sortudo.exception.AuthorizationException;
import by.sortudo.exception.InvalidParameterException;
import by.sortudo.exception.PermissionException;
import by.sortudo.filter.ClientTypeEnum;
import by.sortudo.logic.CommandEnum;
import by.sortudo.logic.validation.ClientValidation;
import by.sortudo.logic.validation.ValidationUtils;
import by.sortudo.servlet.RequestContent;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.Date;
import java.util.List;

/**
 * This class is implementation of interface {@link ActionCommand}. It describes all commands
 * which are relevant to client and received from the client such as add, update,
 * find all, etc. To get information and to add new about client this class consists
 * object {@link ClientDAO}.
 *
 * @author Elena Alekhnovich
 */
public class ClientCommand implements ActionCommand {
    /**
     * Object of DAO class for working with database's entity client.
     */
    private ClientDAO dbConnection;
    /**
     * Object for using order's commands.
     */
    private OrderCommand orderCommand;

    /**
     * Default constructor where object of {@link ClientDAO} is created.
     */
    public ClientCommand() {
        dbConnection = ClientDAO.getInstance();
        orderCommand = new OrderCommand();
    }

    /**
     * Method to define command from client and to execute corresponding to this command action.
     * Depending on the received command this method returns required page.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        switch (CommandEnum.valueOf(action.toUpperCase())) {
            case LOGIN:
                page = logIn(request);
                break;
            case UPDATE:
                page = updateClient(request);
                break;
            case UPDATEPASSWORD:
                page = updatePassword(request);
                break;
            case UPDATEBONUS:
                page = updateBonus(request);
                break;
            case FIND:
                page = getClientPage(request);
                break;
            case REGISTRATION:
                page = register();
                break;
            case AUTHORIZATION:
                page = getErrorAuthPage(request);
                break;
            case EXECUTEREGISTER:
                page = addClient(request);
                break;
            default:
                break;
        }
        return page;
    }

    /**
     * Method to getting page of registration.
     *
     * @return page of registration.
     */
    public String register() {
        return PageConfiguration.getProperty("path.page.registration");
    }

    /**
     * Method where depending on the type of client(user, admin) entrance is realized.
     *
     * @param request content of the request.
     * @return required page.
     */

    public String logIn(RequestContent request) {
        String page = null;
        String clientType = request.getParameter("role");
        ClientTypeEnum clientEnum = clientType == null ? ClientTypeEnum.USER : ClientTypeEnum.ADMIN;
        switch (clientEnum) {
            case USER:
                page = userLogIn(request);
                break;
            case ADMIN:
                page = adminLogIn(request);
                break;
        }
        return page;
    }

    /**
     * This method allows to check and realize authorization for admin.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String adminLogIn(RequestContent request) {
        String page;
        String login = request.getParameter("login");
        String password = DigestUtils.md5Hex(request.getParameter("password"));
        String trueLogin = AdminConfiguration.getProperty("admin.login");
        String truePassword = AdminConfiguration.getProperty("admin.password");
        try {
            if (!(login.equals(trueLogin) && password.equals(truePassword))) {
                throw new AuthorizationException();
            }
            request.setCookie("clientType", "ADMIN");
            page = PageConfiguration.getProperty("path.get.main");
        } catch (AuthorizationException e) {
            logger.error("negative argument: " + e);
            page = setErrorAuth(request);
        }
        return page;
    }

    /**
     * This method allows to check and realize authorization for user.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String userLogIn(RequestContent request) {
        String page;
        String password = DigestUtils.md5Hex(request.getParameter("password"));
        Client client = findClient(request);
        try {
            if (client == null || !password.equals(client.getPassword())) {
                throw new AuthorizationException();
            }
            request.setSessionAttribute("clientId", ((Integer) client.getId()).toString());
            request.setCookie("clientId", ((Integer) client.getId()).toString());
            request.setCookie("clientType", "USER");
            page = PageConfiguration.getProperty("path.get.main");
        } catch (AuthorizationException e) {
            logger.error("negative argument: " + e);
            page = setErrorAuth(request);
        }
        return page;
    }

    /**
     * Method for creating new client.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String addClient(RequestContent request) {
        String page;
        Client client = new Client();
        try {
            client = setClient(request, client);
            client.setPassword(DigestUtils.md5Hex(request.getParameter("password")));
            if (!ClientValidation.isValid(client)) {
                throw new InvalidParameterException("Parameters of client are invalid");
            }
            if(dbConnection.getClientByLogin(client.getLogin()) != null) {
                throw new AuthorizationException("User with such login already exists");
            }
            dbConnection.create(client);
            page = PageConfiguration.getProperty("path.get.index");
        } catch (InvalidParameterException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        } catch (AuthorizationException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorAuthorization");
        }
        return page;
    }

    /**
     * Method for updating client.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String updateClient(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        try {
            if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.ADMIN)) {
                throw new PermissionException("No permissions for this action");
            }
            int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
            Client client = dbConnection.getEntityById(clientId);
            client = setClient(request, client);
            if (!ClientValidation.isValid(client)) {
                throw new InvalidParameterException("Parameters of client are invalid");
            }
            dbConnection.update(client);
            page = PageConfiguration.getProperty("path.get.main");
        } catch (InvalidParameterException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        } catch (PermissionException e) {
            logger.error("negative argument: " + e);
        }
        return page;
    }

    /**
     * This method is for defining required main page.
     * Depending on the type of client(admin, user) this method returns corresponding page.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String getClientPage(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        switch (ClientTypeEnum.valueOf(clientType.toUpperCase())) {
            case USER:
                page = getUserPage(request);
                break;
            case ADMIN:
                page = getAdminPage(request);
                break;
        }
        return page;
    }

    /**
     * Method sets required attributes and returns main page for user.
     *
     * @param request content of the request.
     * @return main page for user.
     */
    public String getUserPage(RequestContent request) {
        int id = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        Client client = dbConnection.getEntityById(id);
        client.setOrderList(orderCommand.getClientsAudio(request));
        request.setAttribute("client", client);
        return PageConfiguration.getProperty("path.page.mainUser");
    }

    /**
     * Method sets required attributes and returns main page for admin.
     *
     * @param request content of the request.
     * @return main page for admin.
     */
    public String getAdminPage(RequestContent request) {
        List<Client> listClient;
        String page;
        String strFind = request.getParameter("clientFind");
        if (StringUtils.isEmpty(strFind)) {
            strFind = "";
        }
        try {
            if (!ValidationUtils.isValid(strFind)) {
                throw new InvalidParameterException("Parameter " + strFind + " is invalid");
            }
            listClient = dbConnection.findAll(strFind);
            request.setAttribute("listBonus", new BonusCommand().getAll());
            request.setAttribute("clientFind", strFind);
            request.setAttribute("clients", listClient);
            ActionCommand.setPages(request, listClient.size(), 4);
            page = PageConfiguration.getProperty("path.page.mainAdmin");
        } catch (InvalidParameterException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method for updating client's password.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String updatePassword(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        try {
            if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.ADMIN)) {
                throw new PermissionException("No permissions for this action");
            }
            int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
            Client client = dbConnection.getEntityById(clientId);
            client.setPassword(DigestUtils.md5Hex(request.getParameter("password")));
            if (!ClientValidation.isValid(client)) {
                throw new InvalidParameterException("Parameters of client are invalid");
            }
            dbConnection.update(client);
            page = PageConfiguration.getProperty("path.get.main");
        } catch (InvalidParameterException | NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        } catch (PermissionException e) {
            logger.error("negative argument: " + e);
        }
        return page;
    }

    /**
     * Method for updating client's bonus.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String updateBonus(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        try {
            if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.USER)) {
                throw new PermissionException("No permissions for this action");
            }
            Bonus bonus = new Bonus();
            int clientId = Integer.valueOf(request.getParameter("clientId"));
            String pageNumber = "&pageButton=" + (request.getParameter("page") == null ? 1 :
                    request.getParameter("page"));
            String strFind = request.getParameter("clientFind");
            Client client = dbConnection.getEntityById(clientId);
            bonus.setId(Integer.parseInt(request.getParameter("bonusId")));
            client.setBonus(bonus);
            dbConnection.update(client);
            page = PageConfiguration.getProperty("path.get.mainAdmin") + strFind + pageNumber;
        } catch (NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        } catch (PermissionException e) {
            logger.error("negative argument: " + e);
        }
        return page;
    }

    /**
     * Method allows to find client by his login.
     *
     * @param request content of the request.
     * @return found client's object.
     */
    public Client findClient(RequestContent request) {
        String login = request.getParameter("login");
        return dbConnection.getClientByLogin(login);
    }

    /**
     * Method that sets error in case of wrong entered authorization's data.
     *
     * @param request content of the request.
     * @return page with error.
     */
    private String setErrorAuth(RequestContent request) {
        return PageConfiguration.getProperty("path.get.index.error")
                + request.getParameter("login");
    }

    /**
     * Method that gets authorization page in case of wrong entered authorization's data.
     *
     * @param request content of the request.
     * @return page with error.
     */
    public String getErrorAuthPage(RequestContent request){
        String login = request.getParameter("login");
        request.setAttribute("error", "Неверный логин или пароль");
        request.setAttribute("login", login);
        return PageConfiguration.getProperty("path.page.index");
    }

    /**
     * Method for getting data from request's content and updating {@link Client} object.
     *
     * @param request content of the request.
     * @param client  created client's object.
     * @return updated client's object.
     */
    private Client setClient(RequestContent request, Client client) {
        client.setName(request.getParameter("name"));
        client.setLogin(request.getParameter("login"));
        client.setDateBirth(Date.valueOf(request.getParameter("dateBirth")));
        client.setEmail(request.getParameter("email"));
        client.setCreditCard(request.getParameter("creditCard"));
        return client;
    }
}
