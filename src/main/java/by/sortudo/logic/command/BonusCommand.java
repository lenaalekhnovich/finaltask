package by.sortudo.logic.command;

import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.BonusDAO;
import by.sortudo.exception.InvalidParameterException;
import by.sortudo.filter.ClientTypeEnum;
import by.sortudo.logic.CommandEnum;
import by.sortudo.bean.Bonus;
import by.sortudo.servlet.RequestContent;
import by.sortudo.logic.validation.BonusValidation;

import java.util.List;

/**
 * This class is implementation of interface {@link ActionCommand}. It describes all commands
 * which are relevant to bonus and received from the client such as add, find all. To get
 * information and to add new about bonus this class consists object {@link BonusDAO}.
 *
 * @author Elena Alekhnovich
 */
public class BonusCommand implements ActionCommand {
    /**
     * Object of DAO class for working with database's entity bonus.
     */
    private BonusDAO dbConnection;

    /**
     * Default constructor where object of {@link BonusDAO} is created.
     */
    public BonusCommand() {
        dbConnection = BonusDAO.getInstance();
    }

    /**
     * Method to define command from client and to execute corresponding to this command action.
     * Depending on the received command this method returns required page.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        switch (CommandEnum.valueOf(action.toUpperCase())) {
            case ADD:
                page = addBonus(request);
                break;
        }
        return page;
    }

    /**
     * Method for creating new bonus.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String addBonus(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.USER)) {
            return page;
        }
        try {
            Bonus bonus = new Bonus();
            bonus.setName(request.getParameter("name"));
            bonus.setSize(Integer.parseInt(request.getParameter("size")));
            bonus.setUnits(request.getParameter("units"));
            if (!BonusValidation.isValid(bonus)) {
                throw new InvalidParameterException("Parameters of bonus are invalid");
            }
            dbConnection.create(bonus);
            page = PageConfiguration.getProperty("path.get.main");
        } catch (InvalidParameterException | NumberFormatException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        }
        return page;
    }

    /**
     * Method for getting all bonuses.
     *
     * @return required page.
     */
    public List<Bonus> getAll() {
        return dbConnection.getAll();
    }
}
