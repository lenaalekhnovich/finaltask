package by.sortudo.logic.command;

import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.ReviewDAO;
import by.sortudo.exception.InvalidParameterException;
import by.sortudo.exception.PermissionException;
import by.sortudo.filter.ClientTypeEnum;
import by.sortudo.logic.CommandEnum;
import by.sortudo.bean.Client;
import by.sortudo.bean.Review;
import by.sortudo.servlet.RequestContent;
import by.sortudo.logic.validation.ValidationUtils;

import java.sql.Date;

/**
 * This class is implementation of interface {@link ActionCommand}. It describes commands
 * which are relevant to review and received from the client. To get information and to add
 * new about review this class consists object {@link ReviewDAO}.
 *
 * @author Elena Alekhnovich
 */
public class ReviewCommand implements ActionCommand {
    /**
     * Object of DAO class for working with database's entity review.
     */
    private ReviewDAO dbConnection;

    /**
     * Default constructor where object of {@link ReviewDAO} is created.
     */
    public ReviewCommand() {
        dbConnection = ReviewDAO.getInstance();
    }

    /**
     * Method to define command from client and to execute corresponding to this command action.
     * Depending on the received command this method returns required page.
     *
     * @param request content of the request.
     * @return required page.
     */
    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        switch (CommandEnum.valueOf(action.toUpperCase())) {
            case ADD:
                page = add(request);
                break;
        }
        return page;
    }

    /**
     * Method for creating new audio track.
     *
     * @param request content of the request.
     * @return required page.
     */
    public String add(RequestContent request) {
        String page = null;
        String clientType = (String) request.getSessionAttribute("clientType");
        try {
            if (ClientTypeEnum.valueOf(clientType.toUpperCase()).equals(ClientTypeEnum.ADMIN)) {
                throw new PermissionException("No permissions for this action");
            }
            Review review = setReview(request);
            if (!ValidationUtils.isValid(review.getContent())) {
                throw new InvalidParameterException("Parameter of review is invalid");
            }
            if (dbConnection.getEntityById(review.getClient().getId(), review.getAudiotrackId()) == null) {
                dbConnection.create(review);
            } else {
                dbConnection.update(review);
            }
            //page = (new AudioTrackCommand().getAudioPage(request));
            page = PageConfiguration.getProperty("path.get.audioInfo") + request.getSessionAttribute("audioId");
        } catch (InvalidParameterException e) {
            logger.error("negative argument: " + e);
            page = PageConfiguration.getProperty("path.page.errorValid");
        } catch (PermissionException e) {
            logger.error("negative argument: " + e);
        }
        return page;
    }

    /**
     * Method for getting data from request's content and creating new {@link Review} object.
     *
     * @param request content of the request.
     * @return updated review's object.
     */
    public Review setReview(RequestContent request) {
        int audioId = Integer.parseInt(request.getSessionAttribute("audioId").toString());
        int clientId = Integer.valueOf((String) request.getSessionAttribute("clientId"));
        String content = request.getParameter("reviewText");
        Review review = new Review();
        Client client = new Client();
        client.setId(clientId);
        review.setClient(client);
        review.setAudiotrackId(audioId);
        review.setContent(content);
        review.setDate(new Date(System.currentTimeMillis()));
        return review;
    }
}
