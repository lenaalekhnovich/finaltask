package by.sortudo.logic;

/**
 * Enum class which consists all possible languages that client can use.
 *
 * @author Elena Alekhnovich
 */
public enum LocaleEnum {
    RU("ru"), EN("en_US"), PL("pl");

    String locale;

    LocaleEnum(String loc){
        this.locale = loc;
    }

    public String getLocale() {
        return locale;
    }
}
