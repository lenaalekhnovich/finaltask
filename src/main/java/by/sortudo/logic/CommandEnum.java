package by.sortudo.logic;

/**
 * Enum class which consists all possible commands that can come from client.
 *
 * @author Elena Alekhnovich
 */
public enum CommandEnum {
    ADD,
    UPDATE,
    DELETE,
    FIND,
    GET,
    LOGIN,
    UPDATEPASSWORD,
    REGISTRATION,
    AUTHORIZATION,
    EXECUTEREGISTER,
    RECOMMENDS,
    ADDRECOMMENDS,
    UPDATEBONUS;
}
