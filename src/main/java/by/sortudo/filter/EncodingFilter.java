package by.sortudo.filter;

import by.sortudo.servlet.Controller;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

/**
 * This class is a filter for servlet {@link Controller}. It controls
 * encoding of the request and response to use right encoding(UTF-8)
 * and to avoid exceptions.
 *
 * @author Elena Alekhnovich
 */
@WebFilter(urlPatterns = {"/audio_store"}, servletNames = {"Controller"},
        initParams = {
                @WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param")})
public class EncodingFilter implements Filter {
    /**
     * String that consists right encoding of the servlet.
     */
    private String encoding;

    /**
     * Implemented method to init filter. In this method encoding is specified.
     *
     * @param filterConfig configuration of the filter.
     * @throws ServletException in case of exception during initialization.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("encoding");
    }

    /**
     * In this method the encoding value is passed to request and response
     * of the servlet.
     *
     * @param servletRequest  request of the servlet.
     * @param servletResponse response of the servlet.
     * @param filterChain     filter chain.
     * @throws IOException      in case of I/O exception has occurred.
     * @throws ServletException in case of servlet's exception.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        servletRequest.setCharacterEncoding(encoding);
        servletResponse.setCharacterEncoding(encoding);
        servletResponse.setContentType("text / html;charset=UTF-8");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
