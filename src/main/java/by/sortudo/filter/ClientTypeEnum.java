package by.sortudo.filter;

/**
 * Enum class that consists types of client.
 *
 * @author Elena Alekhnovich
 */
public enum ClientTypeEnum {
    USER, ADMIN
}
