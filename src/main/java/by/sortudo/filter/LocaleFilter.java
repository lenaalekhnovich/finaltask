package by.sortudo.filter;

import by.sortudo.servlet.Controller;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This class is a filter for servlet {@link Controller}. It controls
 * and sets the right value of the locale.
 *
 * @author Elena Alekhnovich
 */
@WebFilter(urlPatterns = {"/audio_store"}, servletNames = {"Controller"})
public class LocaleFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * In this method filter is trying to get the value of the locale from the session.
     * If this value doesn't exist in session filter set the basic locale.
     *
     * @param servletRequest  request of the servlet.
     * @param servletResponse response of the servlet.
     * @param filterChain     filter chain.
     * @throws IOException      in case of I/O exception has occurred.
     * @throws ServletException in case of servlet's exception.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        String locale = (String) session.getAttribute("locale");
        if (StringUtils.isEmpty(locale)) {
            session.setAttribute("locale", "ru");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
