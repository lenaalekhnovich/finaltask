package by.sortudo.filter;

import by.sortudo.servlet.Controller;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class is a filter for servlet {@link Controller}. It controls
 * access rights of client and his possibility to apply to servlet.
 *
 * @author Elena Alekhnovich
 */
@WebFilter(urlPatterns = {"/audio_store"}, servletNames = {"Controller"})
public class AuthorizationFilter implements Filter {
    /**
     * List that consists commands which filter can skip
     * if the type of client isn't defined.
     */
    private ArrayList<String> possibleCommands;

    /**
     * Implemented method to init filter. In this method list with possible
     * commands is created and required commands are added to this list.
     *
     * @param filterConfig configuration of the filter.
     * @throws ServletException in case of exception during initialization.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        possibleCommands = new ArrayList<>();
        possibleCommands.add("login");
        possibleCommands.add("registration");
        possibleCommands.add("executeRegister");
        possibleCommands.add("changeLocale");
        possibleCommands.add("authorization");
    }

    /**
     * During this method filter is trying to find type of the client in
     * cookie, otherwise filter checks if list with possible commands contains
     * command from client. If the list doesn't contain this command and the
     * type of client is undefined filter break data transfer and return client
     * to the page with authorization.
     *
     * @param servletRequest  request of the servlet.
     * @param servletResponse response of the servlet.
     * @param filterChain     filter chain.
     * @throws IOException      in case of I/O exception has occurred.
     * @throws ServletException in case of servlet's exception.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        String clientType = null;
        String command = request.getParameter("command");
        if (StringUtils.isEmpty(command)) {
            command = "";
        }
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("clientId")) {
                    session.setAttribute("clientId", cookie.getValue());
                }
                if (cookie.getName().equals("clientType")) {
                    clientType = cookie.getValue();
                    session.setAttribute("clientType", clientType);
                }
            }
        }
        if (StringUtils.isEmpty(clientType) && !possibleCommands.contains(command)) {
            RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/");
            dispatcher.forward(request, response);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
