package by.sortudo.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Bean class that consists information about audio track and set/get methods for its parameters.
 *
 * @author Elena Alekhnovich
 */
public class AudioTrack implements Serializable{

    private int id;
    private String name;
    private String author;
    private int year;
    private String genre;
    private float price;
    private String site;
    private List<Review> reviewList = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AudioTrack)) return false;

        AudioTrack that = (AudioTrack) o;

        if (id != that.id) return false;
        if (year != that.year) return false;
        if (Float.compare(that.price, price) != 0) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (genre != null ? !genre.equals(that.genre) : that.genre != null) return false;
        if (site != null ? !site.equals(that.site) : that.site != null) return false;
        return reviewList != null ? reviewList.equals(that.reviewList) : that.reviewList == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        result = 31 * result + (site != null ? site.hashCode() : 0);
        result = 31 * result + (reviewList != null ? reviewList.hashCode() : 0);
        return result;
    }
}
