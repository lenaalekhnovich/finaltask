package by.sortudo.bean;

import java.io.Serializable;

/**
 * Bean class that consists information about bonus and set/get methods for its parameters.
 *
 * @author Elena Alekhnovich
 */
public class Bonus implements Serializable {

    private int id;
    private String name;
    private int size;
    private String units;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bonus)) return false;

        Bonus bonus = (Bonus) o;

        if (id != bonus.id) return false;
        if (size != bonus.size) return false;
        if (name != null ? !name.equals(bonus.name) : bonus.name != null) return false;
        return units != null ? units.equals(bonus.units) : bonus.units == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + size;
        result = 31 * result + (units != null ? units.hashCode() : 0);
        return result;
    }
}
