package by.sortudo.bean;

import java.io.Serializable;

/**
 * Bean class that consists information about order and set/get methods for its parameters.
 *
 * @author Elena Alekhnovich
 */
public class Order implements Serializable {

    private AudioTrack audioTrack;
    private String state;
    private int clientId;

    public AudioTrack getAudioTrack() {
        return audioTrack;
    }

    public void setAudioTrack(AudioTrack audioTrack) {
        this.audioTrack = audioTrack;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int id) {
        this.clientId = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (clientId != order.clientId) return false;
        if (audioTrack != null ? !audioTrack.equals(order.audioTrack) : order.audioTrack != null) return false;
        return state != null ? state.equals(order.state) : order.state == null;
    }

    @Override
    public int hashCode() {
        int result = audioTrack != null ? audioTrack.hashCode() : 0;
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + clientId;
        return result;
    }
}
