package by.sortudo.bean;

import java.io.Serializable;
import java.sql.Date;

/**
 * Bean class that consists information about review and set/get methods for its parameters.
 *
 * @author Elena Alekhnovich
 */
public class Review implements Serializable{

    private int audiotrackId;
    private Client client;
    private String content;
    private Date date;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getAudiotrackId() {
        return audiotrackId;
    }

    public void setAudiotrackId(int audiotrackId) {
        this.audiotrackId = audiotrackId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Review)) return false;

        Review review = (Review) o;

        if (audiotrackId != review.audiotrackId) return false;
        if (client != null ? !client.equals(review.client) : review.client != null) return false;
        if (content != null ? !content.equals(review.content) : review.content != null) return false;
        return date != null ? date.equals(review.date) : review.date == null;
    }

    @Override
    public int hashCode() {
        int result = audiotrackId;
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
