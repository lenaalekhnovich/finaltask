package by.sortudo.database;

import by.sortudo.bean.Bonus;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is an implementation of basic DAO class.
 * It consists methods to work with database's table 'bonus'.
 *
 * @author Elena Alekhnovich
 * @see AbstractDAO
 */
public class BonusDAO extends AbstractDAO<Bonus> {

    public static final String SQL_SELECT_ALL = "SELECT * FROM bonus ORDER BY bonus_name";
    public static final String SQL_INSERT = "INSERT INTO bonus(bonus_name, size, units) VALUES(?,?,?)";
    /**
     * Instance of the class.
     */
    private static volatile BonusDAO instance = null;
    /**
     * Object of {@link ReentrantLock} class to control getting resource by
     * multiple threads.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * Default private constructor for impossibility of creating objects outside.
     */
    private BonusDAO() {
    }

    /**
     * Method that allows to make this class thread-safe Singleton.
     * It creates connection pool's instance if it doesn't exist or gets existing one.
     *
     * @return instance of the class
     */
    public static BonusDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new BonusDAO();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Implementation of the abstract method to create Bonus entity in the database.
     *
     * @param entity entity for creating in the database's table.
     * @return {@code true} if creating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean create(Bonus entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_INSERT)) {
            preparedSt.setString(1, entity.getName());
            preparedSt.setInt(2, entity.getSize());
            preparedSt.setString(3, entity.getUnits());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Method to get all Bonus entities from the database
     *
     * @return the list with found Bonus entities.
     */
    public List<Bonus> getAll() {
        List<Bonus> listBonus = new LinkedList<>();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
            while (resultSet.next()) {
                Bonus bonus = new Bonus();
                bonus.setId(resultSet.getInt("bonus_id"));
                bonus.setName(resultSet.getString("bonus_name"));
                bonus.setSize(resultSet.getInt("size"));
                bonus.setUnits(resultSet.getString("units"));
                listBonus.add(bonus);
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return listBonus;
    }

    @Override
    public Bonus getEntityById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean update(Bonus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Bonus entity) {
        throw new UnsupportedOperationException();
    }


}
