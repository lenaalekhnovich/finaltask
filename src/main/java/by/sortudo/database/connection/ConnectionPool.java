package by.sortudo.database.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This is class of connection pool. Using {@link ArrayBlockingQueue}
 * and {@link Lock} makes this connection pool thread-safe. It
 * consists of methods that allows to get and close connections
 * from the creating pool.
 *
 * @author Elena Alekhnovich
 */
public class ConnectionPool {
    /**
     * Object for writing logs to a file in case of exceptions.
     */
    static Logger logger = LogManager.getLogger(ConnectionPool.class);
    /**
     * Object of {@link ReentrantLock} class to control getting resource by
     * multiple threads.
     */
    private static Lock locking = new ReentrantLock();
    /**
     * Instance of the class.
     */
    private static volatile ConnectionPool instance = null;
    /**
     * Thread-safe queue to creating and getting connections from it.
     */
    private ArrayBlockingQueue<ConnectionWrapper> connectionQueue;
    private String url;
    private String driverName;
    private String user;
    private String pass;
    private int poolSize;

    /**
     * Default private constructor which includes setting all information
     * about database configuration from the property file.
     */
    private ConnectionPool() {
        ResourceBundle resource = ResourceBundle.getBundle("database");
        url = resource.getString("db.url");
        user = resource.getString("db.user");
        pass = resource.getString("db.password");
        driverName = resource.getString("db.driver");
        try {
            poolSize = Integer.parseInt(resource.getString("db.poolSize"));
        } catch (NumberFormatException e) {
            logger.error("negative argument: " + e);
            poolSize = 10;
        }
        initPool();
    }

    /**
     * Method that allows to make this class Singleton. It creates
     * connection pool's instance if it doesn't exist or gets existing one.
     *
     * @return instance of the class.
     */
    public static ConnectionPool getInstance() {
        if (instance == null) {
            locking.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                }
            } finally {
                locking.unlock();
            }
        }
        return instance;
    }

    /**
     * Method creates and puts required amount of connections to the queue.
     *
     */
    private void initPool() {
        try {
            Class.forName(driverName);
            connectionQueue = new ArrayBlockingQueue<>(poolSize);
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(url, user, pass);
                ConnectionWrapper conWrapper = new ConnectionWrapper(connection);
                connectionQueue.add(conWrapper);
            }
        } catch (ClassNotFoundException | SQLException e) {
            logger.error("negative argument: " + e);
        }
    }

    /**
     * Method of taking connection from the queue with connections.
     *
     * @return received connection.
     */
    public ConnectionWrapper getConnection() {
        ConnectionWrapper connection = null;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            logger.error("negative argument: " + e);
        }
        return connection;
    }

    /**
     * Method returns connection to the queue with connections.
     *
     * @param connection used connection which isn't needed.
     */
    public void releaseConnection(ConnectionWrapper connection) {
        if (connection != null) {
            connectionQueue.offer(connection);
        }
    }

    /**
     * Method of closing all connections which the queue with connections
     * contains.
     */
    public void close() {
        try {
            for (int i = 0; i < connectionQueue.size(); i++) {
                ConnectionWrapper con = connectionQueue.take();
                con.reallyClose();
            }
        } catch (InterruptedException | SQLException e) {
            logger.error("negative argument: " + e);
        }
    }

}
