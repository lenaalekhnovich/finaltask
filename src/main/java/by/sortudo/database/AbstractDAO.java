package by.sortudo.database;

import by.sortudo.database.connection.ConnectionPool;
import org.apache.logging.log4j.*;

/**
 * This abstract class is top of the hierarchy of realization DAO pattern.
 * It considers abstract methods to work with database.
 *
 * @author Elena Alekhnovich
 */
public abstract class AbstractDAO  <E> {
    /**
     * Object of {@link ConnectionPool} class to creating and getting
     * connection with database.
     */
    ConnectionPool connectionPool = ConnectionPool.getInstance();
    /**
     * Object for writing logs to a file in case of exceptions.
     */
    static Logger logger = LogManager.getLogger("by.sortudo.database");

    /**
     * Abstract method to get corresponding entity from the database's table.
     * @param id id of the entity to be searched for.
     * @return found entity or {@code null} if entity with such id doesn't exist.
     */
    public abstract E getEntityById(int id);

    /**
     * Abstract method to update corresponding entity from the database's table.
     * @param entity entity which needs to be updated.
     * @return {@code true} if updating was successfully, otherwise {@code false}.
     */
    public abstract boolean update(E entity);

    /**
     * Abstract method to delete corresponding entity from the database's table.
     * @param entity entity which needs to be deleted from the table.
     * @return {@code true} if deleting was successfully, otherwise {@code false}.
     */
    public abstract boolean delete(E entity);

    /**
     * Abstract method to create entity in the database's table.
     * @param entity entity for creating in the database's table.
     * @return {@code true} if creating was successfully, otherwise {@code false}.
     */
    public abstract boolean create(E entity);

}
