package by.sortudo.database;

import by.sortudo.bean.AudioTrack;
import by.sortudo.bean.Order;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is an implementation of basic DAO class.
 * It consists methods to work with database's table 'order'.
 *
 * @author Elena Alekhnovich
 * @see AbstractDAO
 */
public class OrderDAO extends AbstractDAO<Order> {

    public static final String SQL_INSERT = "INSERT INTO `order`(client_id, audiotrack_id, state) VALUES(?,?,?)";
    public static final String SQL_UPDATE = "UPDATE `order` SET state = 'Оплачен' WHERE client_id = ?";
    public static final String SQL_DELETE = "DELETE FROM `order` WHERE audiotrack_id = ? AND client_id = ?";
    /**
     * Instance of the class.
     */
    private static volatile OrderDAO instance = null;
    /**
     * Object of {@link ReentrantLock} class to control getting resource by
     * multiple threads.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * Default private constructor for impossibility of creating objects outside.
     */
    private OrderDAO() {
    }

    /**
     * Method that allows to make this class thread-safe Singleton.
     * It creates connection pool's instance if it doesn't exist or gets existing one.
     *
     * @return instance of the class
     */
    public static OrderDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new OrderDAO();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Implementation of the abstract method to create Order entity in the database.
     *
     * @param entity entity for creating in the database's table.
     * @return {@code true} if creating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean create(Order entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_INSERT)) {
            preparedSt.setInt(1, entity.getClientId());
            preparedSt.setInt(2, entity.getAudioTrack().getId());
            preparedSt.setString(3, entity.getState());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to update Order entity from the database.
     *
     * @param entity entity which needs to be updated.
     * @return {@code true} if updating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean update(Order entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_UPDATE)) {
            preparedSt.setInt(1, entity.getClientId());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to delete Order entity from the database.
     *
     * @param entity entity which needs to be deleted from the table.
     * @return {@code true} if deleting was successfully, otherwise {@code false}.
     */
    @Override
    public boolean delete(Order entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_DELETE)) {
            preparedSt.setInt(1, entity.getAudioTrack().getId());
            preparedSt.setInt(2, entity.getClientId());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Method to find all orders that client has considering order's state.
     * @param client_id client's id to be searched for.
     * @param state order's state to be searched for.
     * @return list of the Order entities.
     */
    public List<Order> getOrdersByClientId(int client_id, String state) {
        List<Order> orderList = null;
        String SQL_SELECT = "{call getClientOrders (?, ?)}";
        try (Connection connection = connectionPool.getConnection();
             CallableStatement callSt = connection.prepareCall(SQL_SELECT)) {
            callSt.setInt(1, client_id);
            callSt.setString(2, state);
            try (ResultSet resultSet = callSt.executeQuery()) {
                orderList = getResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return orderList;
    }

    /**
     * Method to get list with Order entities from result of the query
     * using {@link ResultSet}.
     *
     * @param resultSet passed ResultSet to get data from database.
     * @return the list with Order entities.
     * @throws SQLException in case of exception during executing database query.
     */
    private List<Order> getResultSet(ResultSet resultSet) throws SQLException {
        List<Order> orderList = new LinkedList<>();
        while (resultSet.next()) {
            Order order = new Order();
            AudioTrack audioTrack = new AudioTrack();
            audioTrack.setId(resultSet.getInt("audiotrack_id"));
            audioTrack.setName(resultSet.getString("audiotrack_name"));
            audioTrack.setAuthor(resultSet.getString("author"));
            audioTrack.setYear(resultSet.getInt("year"));
            audioTrack.setGenre(resultSet.getString("genre"));
            audioTrack.setPrice(resultSet.getFloat("price"));
            audioTrack.setSite(resultSet.getString("site"));
            order.setAudioTrack(audioTrack);
            orderList.add(order);
        }
        return orderList;
    }

    @Override
    public Order getEntityById(int id) {
        throw new UnsupportedOperationException();
    }

}
