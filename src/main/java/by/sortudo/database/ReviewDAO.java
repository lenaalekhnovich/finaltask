package by.sortudo.database;

import by.sortudo.bean.Client;
import by.sortudo.bean.Review;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is an implementation of basic DAO class.
 * It consists methods to work with database's table 'review'.
 *
 * @author Elena Alekhnovich
 * @see AbstractDAO
 */

public class ReviewDAO extends AbstractDAO<Review> {

    public static final String SQL_INSERT = "INSERT INTO `review`(client_id, audiotrack_id, content, date) VALUES(?,?,?,?)";
    public static final String SQL_UPDATE = "UPDATE review SET content = ?, date = ?  WHERE audiotrack_id = ? AND client_id = ?";
    public static final String SQL_SELECT = "SELECT * FROM review WHERE audiotrack_id = ? AND client_id = ?";

    /**
     * Instance of the class.
     */
    private static volatile ReviewDAO instance = null;
    /**
     * Object of {@link ReentrantLock} class to control getting resource by
     * multiple threads.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * Default private constructor for impossibility of creating objects outside.
     */
    private ReviewDAO() {
    }

    /**
     * Method that allows to make this class thread-safe Singleton.
     * It creates connection pool's instance if it doesn't exist or gets existing one.
     *
     * @return instance of the class
     */
    public static ReviewDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ReviewDAO();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Implementation of the abstract method to create Review entity in the database.
     *
     * @param entity entity for creating in the database's table.
     * @return {@code true} if creating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean create(Review entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_INSERT)) {
            preparedSt.setInt(1, entity.getClient().getId());
            preparedSt.setInt(2, entity.getAudiotrackId());
            preparedSt.setString(3, entity.getContent());
            preparedSt.setDate(4, entity.getDate());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;

    }

    /**
     * Implementation of the abstract method to update Review entity from the database.
     *
     * @param entity entity which needs to be updated.
     * @return {@code true} if updating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean update(Review entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_UPDATE)) {
            preparedSt.setString(1, entity.getContent());
            preparedSt.setDate(2, entity.getDate());
            preparedSt.setInt(3, entity.getAudiotrackId());
            preparedSt.setInt(4, entity.getClient().getId());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Method to find review which was left by client about certain audio track.
     * @param clientId client's id to be searched for.
     * @param audioId audio track's id to be searched for.
     * @return found Review entity, otherwise {@code null}.
     */
    public Review getEntityById(int clientId, int audioId) {
        Review review = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_SELECT)) {
            preparedSt.setString(1, Integer.valueOf(audioId).toString());
            preparedSt.setString(2, Integer.valueOf(clientId).toString());
            try (ResultSet resultSet = preparedSt.executeQuery()) {
                if (resultSet.next()) {
                    review = new Review();
                    review.setDate(resultSet.getDate("date"));
                    review.setContent(resultSet.getString("content"));
                }
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return review;
    }

    /**
     * Method to find all Review entities from the database which were left by certain client.
     *
     * @param id client's id.
     * @return the list with found AudioTrack entities.
     */
    public List<Review> findAll(int id) {
        List<Review> reviews = new LinkedList<>();
        String selectSql = "{call findClientReviews (?)}";
        try (Connection connection = connectionPool.getConnection();
             CallableStatement callSt = connection.prepareCall(selectSql)) {
            String idStr = Integer.valueOf(id).toString();
            callSt.setString(1, idStr);
            try (ResultSet resultSet = callSt.executeQuery()) {
                reviews = getResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return reviews;
    }

    /**
     * Method to get list with Review entities from result of the query
     * using {@link ResultSet}.
     *
     * @param resultSet passed ResultSet to get data from database.
     * @return the list with AudioTrack entities.
     * @throws SQLException in case of exception during executing database query.
     */
    private List<Review> getResultSet(ResultSet resultSet) throws SQLException {
        List<Review> reviews = new LinkedList<>();
        while (resultSet.next()) {
            Review review = new Review();
            Client client = new Client();
            client.setName(resultSet.getString("client_name"));
            client.setLogin(resultSet.getString("login"));
            review.setDate(resultSet.getDate("date"));
            review.setContent(resultSet.getString("content"));
            review.setClient(client);
            reviews.add(review);
        }
        return reviews;
    }

    @Override
    public boolean delete(Review entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Review getEntityById(int id) {
        throw new UnsupportedOperationException();
    }
}
