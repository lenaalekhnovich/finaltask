package by.sortudo.database;

import by.sortudo.bean.AudioTrack;
import org.apache.commons.collections.CollectionUtils;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is an implementation of basic DAO class.
 * It consists methods to work with database's table 'audio_track'.
 *
 * @author Elena Alekhnovich
 * @see AbstractDAO
 */
public class AudioTrackDAO extends AbstractDAO<AudioTrack> {

    public static final String SQL_SELECT = "SELECT * FROM audio_track WHERE audiotrack_name LIKE ? " +
            "OR author LIKE ? ORDER BY author";
    public static final String SQL_INSERT = "INSERT INTO audio_track(audiotrack_name, author, year, genre, price," +
            " site) VALUES(?,?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM audio_track WHERE audiotrack_id = ?";
    public static final String SQL_UPDATE = "UPDATE audio_track SET audiotrack_name = ?, author = ?, year = ?, " +
            "genre = ?, price = ?, site = ? WHERE audiotrack_id = ?";
    /**
     * Instance of the class.
     */
    private static volatile AudioTrackDAO instance = null;
    /**
     * Object of {@link ReentrantLock} class to control getting resource by
     * multiple threads.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * Default private constructor for impossibility of creating objects outside.
     */
    private AudioTrackDAO() {
    }

    /**
     * Method that allows to make this class thread-safe Singleton.
     * It creates connection pool's instance if it doesn't exist or gets existing one.
     *
     * @return instance of the class
     */
    public static AudioTrackDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new AudioTrackDAO();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Implementation of the abstract method to create AudioTrack entity in the database.
     *
     * @param entity entity for creating in the database's table.
     * @return {@code true} if creating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean create(AudioTrack entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = getPreparedStatement(connection, SQL_INSERT, entity)) {
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to update AudioTrack entity from the database.
     *
     * @param entity entity which needs to be updated.
     * @return {@code true} if updating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean update(AudioTrack entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = getPreparedStatement(connection, SQL_UPDATE, entity)) {
            preparedSt.setInt(7, entity.getId());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to delete AudioTrack entity from the database.
     *
     * @param entity entity which needs to be deleted from the table.
     * @return {@code true} if deleting was successfully, otherwise {@code false}.
     */
    @Override
    public boolean delete(AudioTrack entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_DELETE)) {
            preparedSt.setInt(1, entity.getId());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to get AudioTrack entity from the database.
     *
     * @param id id of the entity to be searched for.
     * @return found entity or {@code null} if entity with such id doesn't exist.
     */
    @Override
    public AudioTrack getEntityById(int id) {
        AudioTrack audioTrack = null;
        String sqlSelect = "SELECT * FROM audio_track WHERE audio_track.audiotrack_id = ?";
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(sqlSelect)) {
            String idStr = Integer.valueOf(id).toString();
            preparedSt.setString(1, idStr);
            try (ResultSet resultSet = preparedSt.executeQuery()) {
                List<AudioTrack> audioList = getResultSet(resultSet);
                if (CollectionUtils.isNotEmpty(audioList)) {
                    audioTrack = audioList.get(0);
                }
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return audioTrack;
    }

    /**
     * Method to find all AudioTrack entities from the database which name or author
     * consider passed string parameter.
     *
     * @param strFind string to be searched for.
     * @return the list with found AudioTrack entities.
     */
    public List<AudioTrack> findAll(String strFind) {
        List<AudioTrack> list = new LinkedList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_SELECT)) {
            String find = "%" + strFind + "%";
            preparedSt.setString(1, find);
            preparedSt.setString(2, find);
            try (ResultSet resultSet = preparedSt.executeQuery()) {
                list = getResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return list;
    }

    /**
     * Method to find AudioTrack entities which name or author consider passed
     * string parameter for client with corresponding id. It consists resolving
     * {@link CallableStatement} to create sql query using stored procedure.
     *
     * @param client_id id of the client to be searched for.
     * @param strFind   string to be searched for.
     * @return the list with found AudioTrack entities.
     */
    public List<AudioTrack> findAudioForClient(int client_id, String strFind) {
        List<AudioTrack> list;
        String selectSql = "{call findAudioForClient (?, ?)}";
        list = findAudio(client_id, strFind, selectSql);
        return list;
    }

    /**
     * Method to find recommended AudioTrack entities which name or author consider passed
     * string parameter for client with corresponding id. It consists resolving
     * {@link CallableStatement} to create sql query using stored procedure.
     *
     * @param client_id id of the client to be searched for.
     * @param strFind   string to be searched for.
     * @return the list with found AudioTrack entities.
     */
    public List<AudioTrack> findRecommendsForClient(int client_id, String strFind) {
        List<AudioTrack> list;
        String selectSql = "{call findRecommendsForClient (?, ?)}";
        list = findAudio(client_id, strFind, selectSql);
        return list;
    }

    /**
     * Method to find AudioTrack entities using passed parameters and string with
     * sql query for database.
     *
     * @param client_id id of the client to be searched for.
     * @param strFind   string to be searched for.
     * @param sql       string with sql query to resolve it in database.
     * @return the list with found AudioTrack entities.
     */
    public List<AudioTrack> findAudio(int client_id, String strFind, String sql) {
        List<AudioTrack> list = null;
        try (Connection connection = connectionPool.getConnection();
             CallableStatement callSt = connection.prepareCall(sql)) {
            callSt.setInt(1, client_id);
            String find = "%" + strFind + "%";
            callSt.setString(2, find);
            try (ResultSet resultSet = callSt.executeQuery()) {
                list = getResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return list;
    }

    /**
     * Method to get list with AudioTrack entities from result of the query
     * using {@link ResultSet}.
     *
     * @param resultSet passed ResultSet to get data from database.
     * @return the list with AudioTrack entities.
     * @throws SQLException in case of exception during executing database query.
     */
    public List<AudioTrack> getResultSet(ResultSet resultSet) throws SQLException {
        List<AudioTrack> list = new LinkedList<>();
        while (resultSet.next()) {
            AudioTrack audioTrack = new AudioTrack();
            audioTrack.setId(resultSet.getInt("audiotrack_id"));
            audioTrack.setName(resultSet.getString("audiotrack_name"));
            audioTrack.setAuthor(resultSet.getString("author"));
            audioTrack.setYear(resultSet.getInt("year"));
            audioTrack.setGenre(resultSet.getString("genre"));
            audioTrack.setPrice(resultSet.getFloat("price"));
            audioTrack.setSite(resultSet.getString("site"));
            list.add(audioTrack);
        }
        return list;
    }

    /**
     * Method to create and get ready prepared statement with AudioTrack entity.
     *
     * @param con    connection to database
     * @param sql    string with sql query
     * @param entity audio track object
     * @return ready prepared statement
     * @throws SQLException in case of setting wrong parameters to query
     */
    public PreparedStatement getPreparedStatement(Connection con, String sql, AudioTrack entity) throws SQLException {
        PreparedStatement preparedSt = con.prepareStatement(sql);
        preparedSt.setString(1, entity.getName());
        preparedSt.setString(2, entity.getAuthor());
        preparedSt.setInt(3, entity.getYear());
        preparedSt.setString(4, entity.getGenre());
        preparedSt.setFloat(5, entity.getPrice());
        preparedSt.setString(6, entity.getSite());
        return preparedSt;
    }
}
