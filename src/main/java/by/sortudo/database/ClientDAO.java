package by.sortudo.database;

import by.sortudo.bean.Bonus;
import by.sortudo.bean.Client;
import org.apache.commons.collections.CollectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is an implementation of basic DAO class.
 * It consists methods to work with database's table 'client'.
 *
 * @author Elena Alekhnovich
 * @see AbstractDAO
 */
public class ClientDAO extends AbstractDAO<Client> {

    public static final String SQL_INSERT = "INSERT INTO client(client_name, date_birth, email, credit_card, login," +
            " password, bonus_id) VALUES(?,?,?,?,?,?,?)";
    public static final String SQL_UPDATE = "UPDATE client SET client_name = ?, date_birth = ?, email = ?, " +
            "credit_card = ?, login = ?, password = ? , bonus_id = ? WHERE client_id = ?";
    public static final String SQL_SELECT = "SELECT * FROM client LEFT JOIN bonus ON client.bonus_id = bonus.bonus_id " +
            "WHERE client_name LIKE ? OR login LIKE ? ORDER BY client_name";
    /**
     * Instance of the class.
     */
    private static volatile ClientDAO instance = null;
    /**
     * Object of {@link ReentrantLock} class to control getting resource by
     * multiple threads.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * Default private constructor for impossibility of creating objects outside.
     */
    private ClientDAO() {
    }

    /**
     * Method that allows to make this class thread-safe Singleton.
     * It creates connection pool's instance if it doesn't exist or gets existing one.
     *
     * @return instance of the class
     */
    public static ClientDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ClientDAO();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Implementation of the abstract method to create Client entity in the database.
     *
     * @param entity entity for creating in the database's table.
     * @return {@code true} if creating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean create(Client entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = getPreparedStatement(connection, SQL_INSERT, entity)) {
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to update Client entity from the database.
     *
     * @param entity entity which needs to be updated.
     * @return {@code true} if updating was successfully, otherwise {@code false}.
     */
    @Override
    public boolean update(Client entity) {
        boolean flag = false;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = getPreparedStatement(connection, SQL_UPDATE, entity)) {
            preparedSt.setInt(8, entity.getId());
            preparedSt.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return flag;
    }

    /**
     * Implementation of the abstract method to get Client entity from the database.
     *
     * @param id id of the entity to be searched for.
     * @return found entity or {@code null} if entity with such id doesn't exist.
     */
    @Override
    public Client getEntityById(int id) {
        Client client = null;
        String sqlSelect = "SELECT * FROM client LEFT JOIN bonus ON client.bonus_id = bonus.bonus_id " +
                "WHERE client_id = ?";
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(sqlSelect)) {
            String idStr = Integer.valueOf(id).toString();
            preparedSt.setString(1, idStr);
            try (ResultSet resultSet = preparedSt.executeQuery()) {
                List<Client> clientList = getResultSet(resultSet);
                if (CollectionUtils.isNotEmpty(clientList)) {
                    client = clientList.get(0);
                }
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return client;
    }

    /**
     * Method to find Client by its login from the database.
     *
     * @param login client's login to be searched for.
     * @return entity Client
     */
    public Client getClientByLogin(String login) {
        Client client = null;
        String sqlSelect = "SELECT * FROM client LEFT JOIN bonus ON client.bonus_id = bonus.bonus_id " +
                "WHERE login = ?";
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(sqlSelect)) {
            preparedSt.setString(1, login);
            try (ResultSet resultSet = preparedSt.executeQuery()) {
                List<Client> clientList = getResultSet(resultSet);
                if (CollectionUtils.isNotEmpty(clientList)) {
                    client = clientList.get(0);
                }
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return client;
    }

    /**
     * Method to find all Client entities from the database which name or login
     * consider passed string parameter.
     *
     * @param strFind string to be searched for.
     * @return the list with found Client entities.
     */
    public List<Client> findAll(String strFind) {
        List<Client> list = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedSt = connection.prepareStatement(SQL_SELECT)) {
            String find = "%" + strFind + "%";
            preparedSt.setString(1, find);
            preparedSt.setString(2, find);
            try (ResultSet resultSet = preparedSt.executeQuery()) {
                list = getResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("negative argument:", e);
        }
        return list;
    }

    @Override
    public boolean delete(Client entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Method to get list with Client entities from result of the query
     * using {@link ResultSet}.
     *
     * @param resultSet passed ResultSet to get data from database.
     * @return the list with Client entities.
     * @throws SQLException in case of exception during executing database query.
     */
    public List<Client> getResultSet(ResultSet resultSet) throws SQLException {
        List<Client> clientList = new LinkedList<>();
        while (resultSet.next()) {
            Client client = new Client();
            client.setId(resultSet.getInt("client_id"));
            client.setName(resultSet.getString("client_name"));
            client.setLogin(resultSet.getString("login"));
            client.setDateBirth(resultSet.getDate("date_birth"));
            client.setEmail(resultSet.getString("email"));
            client.setCreditCard(resultSet.getString("credit_card"));
            client.setPassword(resultSet.getString("password"));
            Bonus bonus = null;
            if (resultSet.getString("bonus_name") != null) {
                bonus = new Bonus();
                bonus.setId(resultSet.getInt("bonus_id"));
                bonus.setName(resultSet.getString("bonus_name"));
                bonus.setSize(resultSet.getInt("size"));
                bonus.setUnits(resultSet.getString("units"));
            }
            client.setBonus(bonus);
            clientList.add(client);
        }
        return clientList;
    }

    /**
     * Method to create and get ready prepared statement with Client entity.
     *
     * @param con    connection to database
     * @param sql    string with sql query
     * @param entity audio track object
     * @return ready prepared statement
     * @throws SQLException in case of setting wrong parameters to query
     */
    public PreparedStatement getPreparedStatement(Connection con, String sql, Client entity) throws SQLException {
        PreparedStatement preparedSt = con.prepareStatement(sql);
        preparedSt.setString(1, entity.getName());
        preparedSt.setDate(2, entity.getDateBirth());
        preparedSt.setString(3, entity.getEmail());
        preparedSt.setString(4, entity.getCreditCard());
        preparedSt.setString(5, entity.getLogin());
        preparedSt.setString(6, entity.getPassword());
        preparedSt.setString(7,
                entity.getBonus() == null ? null : Integer.valueOf(entity.getBonus().getId()).toString());

        return preparedSt;
    }
}
