package by.sortudo.configuration;

import java.util.ResourceBundle;

/**
 * This class contains information about page configuration.
 * It allows to get right page from the property file.
 *
 * @author Elena Alekhnovich
 */
public class PageConfiguration {
    /**
     * Constant object of the class {@link ResourceBundle} which includes
     * page property file.
     */
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    /**
     * Default constructor.
     */
    private PageConfiguration() {
    }

    /**
     * Method allows to get name of the page from the property file by the key.
     * @param key the key to be searched for.
     * @return location of the page that corresponds the key.
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
