package by.sortudo.configuration;

import java.util.ResourceBundle;

/**
 * This class contains information about admin configuration such as
 * login and password. It allows to get admin login and password from
 * the property file.
 *
 * @author Elena Alekhnovich
 */
public class AdminConfiguration {
    /**
     * Constant object of the class {@link ResourceBundle} which includes
     * admin property file.
     */
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("admin");

    /**
     * Default constructor
     */
    private AdminConfiguration() { }

    /**
     * Method allows to get information from the property file by the key.
     * @param key the key to be searched for.
     * @return the value that corresponds the key.
     */
    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
