package by.sortudo.servlet;

import by.sortudo.configuration.PageConfiguration;
import by.sortudo.database.connection.ConnectionPool;
import by.sortudo.logic.ActionFactory;
import by.sortudo.logic.command.ActionCommand;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class of application's servlet. It's possible to apply to this servlet
 * in browser using url '/audio_store'. Consists standard overriding methods
 * to execute requests get and post.
 *
 * @author Elena Alekhnovich
 * @see HttpServlet
 */
@WebServlet("/audio_store")
public class Controller extends HttpServlet {
    /**
     * Method for processing get-request.
     *
     * @param request  request of the servlet.
     * @param response response of the servlet.
     * @throws IOException      in case of I/O exception has occurred.
     * @throws ServletException in case of servlet's exception.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = processRequest(request, response);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    /**
     * Method for processing post-request.
     *
     * @param request  request of the servlet.
     * @param response response of the servlet.
     * @throws IOException      in case of I/O exception has occurred.
     * @throws ServletException in case of servlet's exception.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = processRequest(request, response);
        response.sendRedirect(page);
    }

    /**
     * Method for processing get and post requests. It creates factory where the command
     * which came from client is defined, after that client's request is executed
     * and this method received required page. If this page is undefined or {@code null}
     * the page with error will open for client using {@link RequestDispatcher}.
     *
     * @param request  request of the servlet.
     * @param response response of the servlet.
     * @throws IOException      in case of I/O exception has occurred.
     * @throws ServletException in case of servlet's exception.
     */
    private String processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        RequestContent requestContent = new RequestContent();
        requestContent.insertAttributes(request, response);
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(requestContent);
        page = command.execute(requestContent);
        if (StringUtils.isNotEmpty(page)) {
            requestContent.extractValues(request);
            requestContent.extractCookies(response);
        } else {
            page = PageConfiguration.getProperty("path.page.error");
        }
        return page;
    }

    /**
     * Method that executes when servlet is destroying. Consists closing all
     * connections in connection pool.
     */
    public void destroy() {
        ConnectionPool.getInstance().close();
    }

}