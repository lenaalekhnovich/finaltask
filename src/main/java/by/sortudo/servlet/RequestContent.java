package by.sortudo.servlet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is the wrapper of the servlet's request. It consists content of the request:
 * attributes, parameters,session attributes and also methods to insert and extract
 * this values.
 *
 * @author Elena Alekhnovich
 */
public class RequestContent {
    /**
     * Map that consists attributes from the request.
     */
    private HashMap<String, Object> requestAttributes;
    /**
     * Map that consists parameters from the request.
     */
    private HashMap<String, String> requestParameters;
    /**
     * Map that consists session attributes from the request.
     */
    private HashMap<String, Object> sessionAttributes;
    /**
     * Map that consists cookies from the request.
     */
    private HashMap<String, String> cookies;

    /**
     * Default constructor.
     */
    public RequestContent() {
        requestAttributes = new HashMap<>();
        requestParameters = new HashMap<>();
        sessionAttributes = new HashMap<>();
        cookies = new HashMap<>();
    }

    /**
     * Method to set values such as attributes and session attributes to request.
     *
     * @param request servlet request.
     */
    void extractValues(HttpServletRequest request) {
        HttpSession session = request.getSession();
        for (Map.Entry entry : requestAttributes.entrySet()) {
            request.setAttribute((String) entry.getKey(), entry.getValue());
        }
        for (Map.Entry entry : sessionAttributes.entrySet()) {
            session.setAttribute((String) entry.getKey(), entry.getValue());
        }
    }

    /**
     * Method to set cookies to response.
     *
     * @param response servlet request.
     */
    void extractCookies(HttpServletResponse response) {
        for (Map.Entry entry : cookies.entrySet()) {
            Cookie cookie = new Cookie((String) entry.getKey(), (String) entry.getValue());
            response.addCookie(cookie);
        }
    }

    /**
     * Method to insert attributes and parameters from servlet request
     * and add to this class.
     *
     * @param request  servlet request.
     * @param response servlet response.
     * @throws UnsupportedEncodingException in case of unsupported character encoding.
     */
    void insertAttributes(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String name;
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            name = paramNames.nextElement();
            String value = new String(request.getParameter(name).getBytes("ISO-8859-1"), "UTF-8");
            requestParameters.put(name, value);
        }

        Enumeration<String> attrNames = request.getAttributeNames();
        while (attrNames.hasMoreElements()) {
            name = attrNames.nextElement();
            requestAttributes.put(name, request.getAttribute(name));
        }
        Enumeration<String> sessionNames = request.getSession(false).getAttributeNames();
        while (sessionNames.hasMoreElements()) {
            name = sessionNames.nextElement();
            sessionAttributes.put(name, request.getSession().getAttribute(name));
        }
        Cookie[] cookiesContent = request.getCookies();
        for (Cookie cookie : cookiesContent) {
            cookies.put(cookie.getName(), cookie.getValue());
        }
    }

    public String getParameter(String name) {
        return requestParameters.get(name);
    }

    public Object getSessionAttribute(String name) {
        return sessionAttributes.get(name);
    }

    public void setAttribute(String name, Object value) {
        requestAttributes.put(name, value);
    }

    public void setCookie(String name, String value) {
        cookies.put(name, value);
    }

    public void setSessionAttribute(String name, Object value) {
        sessionAttributes.put(name, value);
    }

    public String getCookie(String name) {
        return cookies.get(name);
    }

    public void setParameter(String key, String name){requestParameters.put(key, name);}

    public Object getAttribute(String name) {
        return requestAttributes.get(name);
    }

}
