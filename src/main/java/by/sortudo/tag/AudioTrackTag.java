package by.sortudo.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag for audio track's content. It consists audio track's attributes
 * such as id, name, author and price of the audio track. Allows to show
 * to client information about audio track.
 *
 * @author Elena Alekhnovich
 */
@SuppressWarnings("serial")
public class AudioTrackTag extends TagSupport {
    private String id;
    private String name;
    private String author;
    private String price;

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.write("<div class='audio-block' id=" + id + ">");
            out.write("<div class='audio-name'> <span>" + author + " - " + name + "</span> </div>");
            out.write("<div class='info-img'> <a href=''> " +
                    "<img class='info-img' src='/resource/images/information-icon.png'> </a> </div>");
            out.write("<div class='audio-price'> <span name='price'>" + price + " $</span> </div>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getPrice() {
        return price;
    }
}
