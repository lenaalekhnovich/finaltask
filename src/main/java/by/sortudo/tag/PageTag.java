package by.sortudo.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag for pages. It consists attribute that includes amount of pages.
 * Allows to show to client amount of the pages with information
 * and to navigate through them.
 *
 * @author Elena Alekhnovich
 */
public class PageTag extends TagSupport {

    private int pageSize;

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.write("<div class='audio-block'>");
            if (pageSize > 1) {
                for (int i = 1; i <= pageSize; i++) {
                    out.write("<input type='button' name='pageButton' class='page-button' value='" + i + "'>");
                }
            }
            out.write("</div>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    public String getPageSize() {
        return Integer.toString(pageSize);
    }

    public void setPageSize(String pageSize) {
        this.pageSize = Integer.parseInt(pageSize);
    }
}
