package by.sortudo.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag for locale's content. Allows to show to client information about different languages
 * which he can use and change during working with the application.
 *
 * @author Elena Alekhnovich
 */
public class LocaleTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.write("<div class='locale-block'>");
            out.write(" <img src='../resource/images/local.png'><ul>");
            out.write("<li><a href='/audio_store?commandObject=locale&command=changeLocale&lang=ru'>RU</a></li>");
            out.write("<li><a href='/audio_store?commandObject=locale&command=changeLocale&lang=en'>EN</a></li>");
            out.write("<li><a href='/audio_store?commandObject=locale&command=changeLocale&lang=pl'>PL</a></li>");
            out.write("</ul></div>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

}
