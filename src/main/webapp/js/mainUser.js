/**
 * Created by Босс on 03.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]');

$('.button-group').on('click', function (event) {
    switch (event.target.value) {
        case 'Edit personal information':
        case 'Edytować informacje osobiste':
        case 'Редактировать данные':
            $('#name').val($('#spanName').html());
            $('#login').val($('#spanLogin').html());
            $('#date').val($('#spanDate').html());
            $('#creditCard').val($('#spanCredit').html());
            $('#email').val($('#spanEmail').html());
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalClient')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Change password':
        case 'Zmienić hasło':
        case 'Изменить пароль':
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalPassword')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Exit':
        case 'Wyjście':
        case 'Выход':
            var cookie_date = new Date();
            cookie_date.setTime(cookie_date.getTime() - 1);
            document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
            document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
            break;

    }
});

$('.info-img').on('click', function (event) {
    var hrefAudio = event.target.parentElement,
        id = hrefAudio.parentElement.getAttribute("id");
    hrefAudio.href = '/audio_store?commandObject=audioTrack&command=get&audioId='+id;
});

$('#modalPassword').on('click', function (event) {
    switch (event.target.value) {
        case 'Save':
        case 'Zachować':
        case 'Сохранить':
            if (validatePassword()) {
                command.val("updatePassword");
                commandObj.val("client");
                return true;
            }
            return false;
    }
});

$('#modalClient').on('click', function (event) {
    switch (event.target.value) {
        case 'Save':
        case 'Zachować':
        case 'Сохранить':
            if (validateClient()) {
                command.val("update");
                commandObj.val("client");
                return true;
            }
            return false;
    }
});


