/**
 * Created by Босс on 07.05.2017.
 */
function exit() {
    var cookie_date = new Date();
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
    document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

$('.modal_close, #overlay').click(function () {
    $('.modal-form')
        .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
});