
/**
 * Created by Босс on 03.04.2017.
 */
function validateClient(){
    var login = document.getElementById('login'),
        name = document.getElementById('name'),
        date = document.getElementById('date'),
        email = document.getElementById('email'),
        creditCard =  document.getElementById('creditCard'),
        check = true;
    if(!checkName(name)){
        check = false;
    }
    if(!checkLogin(login)){
        check = false;
    }
    if(!checkDate(date)){
        check = false;
    }
    if(!checkEmail(email)){
        check = false;
    }
    if(!checkCreditCard(creditCard)){
        check = false;
    }
    return check;
}

function validatePassword(){
    var password = document.getElementById('password'),
        passwordRep = document.getElementById('passwordRepeat'),
        oldPassword = document.getElementById('oldPassword'),
        trueOldPassword = document.getElementsByName('trueOldPassword')[0],
        check = true;
    if(!checkPassword(password)){
        check = false;
    }
    if(!checkPasswordRep(password, passwordRep)){
        check = false;
    }
    if(!checkTruePassword(oldPassword, trueOldPassword)){
        check = false;
    }
    return check;
}

function checkCreditCard(creditCard){
    var error = document.getElementsByName('errorCredit')[0],
        regExp = /^\d{10,16}$/,
        creditValue = creditCard.value;
    error.style.display = 'none';
    if(!toggleInput(creditCard)){
        return false;
    }
    if(!regExp.test(creditValue)){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkName(name){
    var error = document.getElementsByName('errorName')[0],
        errorFirst = document.getElementsByName('errorNameFirst')[0],
        regExpFirstLetter = /^[А-ЯA-Z]/,
        regExp = /^[А-ЯA-Z][а-яa-z]+$/,
        nameValue = name.value;
    error.style.display = 'none';
    errorFirst.style.display = 'none';
    if(!toggleInput(name)){
        return false;
    }
    if(!regExpFirstLetter.test(nameValue)){
        errorFirst.style.display = 'inline-block';
        return false;
    }
    if(!regExp.test(nameValue)){
        error.append(': а-я,a-z');
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkLogin(login){
    var error = document.getElementsByName('errorLogin')[0],
        errorLength = document.getElementsByName('errorLoginLength')[0],
        regExp = /^[\w]+$/i,
        loginValue = login.value;
    error.style.display = 'none';
    errorLength.style.display = 'none';
    if(!toggleInput(login)){
        return false;
    }
    if(loginValue.length < 5){
        errorLength.append(' 5');
        errorLength.style.display = 'inline-block';
        return false;
    }
    if(!regExp.test(loginValue)){
        error.append(': A-z, 0-9, _');
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkDate(date){
    var error = document.getElementsByName('errorDate')[0],
        yearNow = new Date().getFullYear(),
        yearValue = new Date(date.value).getFullYear();
    error.style.display = 'none';
    if(!toggleInput(date)){
        return false;
    }
    if((yearNow - yearValue < 7) || (yearNow - yearValue > 120)){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkEmail(email){
    var error = document.getElementsByName('errorEmail')[0],
        regExp = /^[\wа-яА-Я-.]+@[a-zа-я]{2,5}\.[a-zа-я]{2,3}$/i,
        emailValue = email.value;
    error.style.display = 'none';
    if(!toggleInput(email)){
        return false;
    }
    if(!regExp.test(emailValue)){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkPassword(password){
    var error = document.getElementsByName('errorPassword')[0],
        errorLength = document.getElementsByName('errorPasswordLength')[0],
        pasValue = password.value;
    error.style.display = 'none';
    errorLength.style.display = 'none';
    if(!toggleInput(password)){
        return false;
    }
    if(pasValue.length < 6){
        errorLength.append(' 6');
        errorLength.style.display = 'inline-block';
        return false;
    }
    if(!(pasValue.match(/[A-ZА-Я]/) && pasValue.match(/\d/) && pasValue.match(/[a-zа-я]/))){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkPasswordRep(password, passwordRep){
    var error = document.getElementsByName('errorRepeatPassword')[0],
        pasValue = password.value,
        pasRepValue = passwordRep.value;
    error.style.display = 'none';
    if(!toggleInput(passwordRep)){
        return false;
    }
    if(pasValue != pasRepValue){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkTruePassword(oldPassword, truePassword){
    var error = document.getElementsByName('errorOldPassword')[0],
        pasValue = md5(oldPassword.value),
        pasTrueValue = truePassword.value;
    error.style.display = 'none';
    if(!toggleInput(oldPassword)){
        return false;
    }
    if(pasValue !== pasTrueValue){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function toggleInput(element) {
    if (!element.value) {
        element.style.boxShadow = '0px 0px 5px 5px rgba(255, 178, 178, 1)';
        return false;
    }
    element.style.boxShadow = 'none';
    return true;
}





