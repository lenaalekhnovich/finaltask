/**
 * Created by Босс on 24.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]'),
    clientId = $('[name = clientId]'),
    bonusId = $('[name = bonusId]');

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Add new bonus':
        case 'Dodać nowy bonus':
        case 'Добавить новый бонус':
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalAdd')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Change bonus':
        case 'Zmienić bonus':
        case 'Изменить бонус':
            clientId.val(event.target.parentElement.getAttribute("id"));
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalClient')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Search':
        case 'Poszukiwanie':
        case 'Поиск':
            var clientFind = $('[name=clientFind]').val();
            location.href='/audio_store?commandObject=client&command=find&clientFind=' + clientFind;
            command.val("find");
            commandObj.val("client");
            break;

    }
});

$('.page-button').on('click', function (event) {
    var number = event.target.value;
    var clientFind = $('[name=clientFind]').val();
    location.href='/audio_store?commandObject=client&command=find&clientFind=' + clientFind
        + "&pageButton=" + number;

});

$('#modalClient').on('click', function (event) {
    switch (event.target.value) {
        case 'Save':
        case 'Zachować':
        case 'Сохранить':
            command.val("updateBonus");
            commandObj.val("client");
            bonusId.val($("#select option:selected").val());
            return true;

    }
    return false;
});

$('#modalAdd').on('click', function (event) {
    switch (event.target.value) {
        case 'Save':
        case 'Zachować':
        case 'Сохранить':
            if (validateBonus()) {
                var bonusName = $("#selectName option:selected").text();
                $('#name').val(bonusName);
                command.val("add");
                commandObj.val("bonus");
                return true;

            }
            return false;
    }
});
