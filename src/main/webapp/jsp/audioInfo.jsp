<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.audio" var="audioRB"/>
<fmt:setBundle basename="pagecontent.order" var="orderRB"/>
<fmt:setBundle basename="pagecontent.error" var="errorRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <link rel="stylesheet" type="text/css" href="/css/audioInfo.css">
    <link rel="stylesheet" type="text/css" href="/css/modalForm.css">
    <title><fmt:message key="label.info" bundle="${audioRB}"/></title>
</head>
<body>
<header class="header" method="GET">
    <a href="/audio_store">
        <fmt:message key="header.main" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=audioTrack&command=find">
        <fmt:message key="header.audio" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=order&command=find">
        <fmt:message key="header.basket" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=audioTrack&command=recommends">
        <fmt:message key="header.recommends" bundle="${mainRB}"/>
    </a>
    <a onclick='exit()' href='/audio_store'>
        <fmt:message key='label.exit' bundle='${mainRB}'/>
    </a>
    <ctg:localeTag/>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="audiotrack">
    <input type="hidden" name="command" value="get">
    <div class="tittle">
        <p><fmt:message key="label.info" bundle="${audioRB}"/></p>
    </div>
    <div class="audio-info">
        <p><fmt:message key="label.name" bundle="${audioRB}"/></p>
        <span id="spanName">${audiotrack.name}</span>
        <p><fmt:message key="label.author" bundle="${audioRB}"/></p>
        <span id="spanAuthor">${audiotrack.author}</span>
        <p><fmt:message key="label.year" bundle="${audioRB}"/></p>
        <span id="spanYear">${audiotrack.year}</span>
        <p><fmt:message key="label.genre" bundle="${audioRB}"/></p>
        <span id="spanGenre">${audiotrack.genre}</span>
        <p><fmt:message key="label.price" bundle="${audioRB}"/></p>
        <span id="spanPrice">${audiotrack.price}</span>
    </div>
    <input type="button" id="updateAudio" value="<fmt:message key="button.update" bundle="${audioRB}"/>"
           class="default-button update-button"/>
    <div class="review">
        <textarea name="reviewText"></textarea>
        <span name="errorReviewLength" class="error-message error-content">
            <fmt:message key="error.length" bundle="${errorRB}"/>
        </span>
        <span name="errorReview" class="error-message error-content">
            <fmt:message key="error.symbol" bundle="${errorRB}"/>
        </span>
        <input type="submit" value="<fmt:message key="button.review.add" bundle="${orderRB}"/>" class="default-button"/>
    </div>
    <div class="tittle">
        <p><fmt:message key="label.reviews" bundle="${orderRB}"/></p>
    </div>
    <c:set var="reviews" value="${audiotrack.reviewList}"/>
    <c:choose>
        <c:when test="${not empty reviews}">
            <c:forEach var="elem" items="${audiotrack.reviewList}" begin="${(page-1)*3}" end="${page*3-1}">
                <div class="review-info">
                    <div class="reviewer-info">
                        <span>${elem.client.name}, ${elem.date}</span>
                    </div>
                    <div class="review-content">
                        <span>${elem.content}</span>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="review-info">
                <span><fmt:message key="label.review.notFound" bundle="${orderRB}"/></span>
            </div>
        </c:otherwise>
    </c:choose>
    <ctg:pageTag pageSize="${pageSize}"/>
    <div class="modal-form" id="modalAudio">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="author" class="form-item-label">
                <fmt:message key="label.author" bundle="${audioRB}"/>
            </label>
            <input id="author" class="form-input" name="author">
            <span name="errorAuthor" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="name" class="form-item-label">
                <fmt:message key="label.name" bundle="${audioRB}"/>
            </label>
            <input id="name" class="form-input" name="name">
            <span name="errorName" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="year" class="form-item-label">
                <fmt:message key="label.year" bundle="${audioRB}"/>
            </label>
            <input id="year" class="form-input" name="year">
            <span name="errorYear" class="error-message">
                 <fmt:message key="error.audio.year" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="genre" class="form-item-label">
                <fmt:message key="label.genre" bundle="${audioRB}"/>
            </label>
            <input id="genre" class="form-input" name="genre">
            <span name="errorGenre" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="price" class="form-item-label">
                <fmt:message key="label.price" bundle="${audioRB}"/>
            </label>
            <input id="price" class="form-input" name="price">
            <span name="errorPrice" class="error-message">
                <fmt:message key="error.audio.price" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <input type='submit' value='<fmt:message key="button.save" bundle="${mainRB}"/>' class="default-button">
            <input type='reset' value='<fmt:message key="button.clear" bundle="${mainRB}"/>' class="default-button">
        </div>
    </div>
    <div id="overlay"></div>
</form>
<footer class="footer"><fmt:message key="footer" bundle="${mainRB}"/></footer>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/default.js"></script>
<script src="/js/validation/validationAudio.js"></script>
<script src="/js/audioInfo.js"></script>
</body>
</html>