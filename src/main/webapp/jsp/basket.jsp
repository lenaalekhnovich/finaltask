<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 16.04.2017
  Time: 0:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.order" var="orderRB"/>
<fmt:setBundle basename="pagecontent.bonus" var="bonusRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/audiotracks.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <title><fmt:message key="header.basket" bundle="${mainRB}"/></title>
</head>
<body>
<header class="header" method="GET">
    <a href="/audio_store">
        <fmt:message key="header.main" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=audioTrack&command=find">
        <fmt:message key="header.audio" bundle="${mainRB}"/>
    </a>
    <fmt:message key="header.basket" bundle="${mainRB}"/>
    <a href="/audio_store?commandObject=audioTrack&command=recommends">
        <fmt:message key="header.recommends" bundle="${mainRB}"/>
    </a>
    <a onclick='exit()' href='/audio_store'>
        <fmt:message key='label.exit' bundle='${mainRB}'/>
    </a>
    <ctg:localeTag/>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="order">
    <input type="hidden" name="command" value="find">
    <input type="hidden" name="audioId" value="">
    <c:set var="audioList" value="${audiotracks}"/>
    <c:choose>
        <c:when test="${not empty audioList}">
            <c:forEach var="elem" items="${audiotracks}">
                <ctg:audioTag id="${elem.id}" name="${elem.name}" author="${elem.author}" price="${elem.price}"/>
                <input type="submit" value="<fmt:message key="button.delete" bundle="${mainRB}"/>" class="default-button">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="audio-block">
                <span class="error"><fmt:message key="label.order.notFound" bundle="${orderRB}"/></span>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="bonus-block">
        <input type="checkbox" id="bonusCheck" value="Бонус" name="${client.bonus.name}" size="${client.bonus.size}"/>
        <label for="bonusCheck">
            <fmt:message key="button.use" bundle="${bonusRB}"/>
        </label>
    </div>
    <div class="total-cost">
        <span><fmt:message key="label.totalCost" bundle="${orderRB}"/>:</span>
        <span name="totalCost">0.0 $</span>
        <input type="submit" name="payButton" value="<fmt:message key="button.pay" bundle="${orderRB}"/>"
               class="default-button"/>
    </div>
</form>
<footer class="footer"><fmt:message key="footer" bundle="${mainRB}"/></footer>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/default.js"></script>
<script src="/js/basket.js"></script>
</body>
</html>
