<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.client" var="clientRB"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.bonus" var="bonusRB"/>
<fmt:setBundle basename="pagecontent.error" var="errorRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/mainAdmin.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <link rel="stylesheet" type="text/css" href="/css/modalForm.css">
    <title><fmt:message key="header.main" bundle="${mainRB}"/></title>
<body>
<header class="header">
    <fmt:message key="header.main" bundle="${mainRB}"/>
    <a href="/audio_store?commandObject=audioTrack&command=find">
        <fmt:message key="header.audio" bundle="${mainRB}"/>
    </a>
    <a onclick="exit()" href="/audio_store">
        <fmt:message key="label.exit" bundle="${mainRB}"/>
    </a>
    <ctg:localeTag/>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="client">
    <input type="hidden" name="command" value="find">
    <input type="hidden" name="clientId" value="">
    <input type="hidden" name="bonusId" value="">
    <input type="hidden" name="page" value="${page}">
    <div class="client-block">
        <input type="text" name="clientFind" value="${clientFind}">
        <input type="button" value="<fmt:message key="button.find" bundle="${mainRB}"/>" class="default-button">
        <input type="submit" value="<fmt:message key="button.add" bundle="${bonusRB}"/>" class="add-button default-button">
    </div>
    <c:set var="clientList" value="${clients}"/>
    <c:choose>
        <c:when test="${not empty clientList}">
            <c:forEach var="elem" items="${clients}" begin="${(page-1)*4}" end="${page*4-1}">
                <div class="client-block" id="${elem.id}">
                    <div class="client">
                        <span> ${elem.name} (${elem.login})</span>
                        <c:set var="bonus" value="${elem.bonus}"/>
                        <c:choose>
                            <c:when test="${not empty bonus}">
                                <p id="bonus">
                                    <fmt:message key="label.bonus" bundle="${bonusRB}"/>:
                                    ${elem.bonus.name} ${elem.bonus.size} ${elem.bonus.units}
                                </p>
                            </c:when>
                            <c:otherwise>
                                <p id="bonus">
                                    <fmt:message key="label.notFound" bundle="${bonusRB}"/>
                                </p>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <input type="button" name="addBonus" value="<fmt:message key="button.change" bundle="${bonusRB}"/>" class="default-button">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="client-block">
            <div class="client">
                <span>
                    <fmt:message key="label.notFound" bundle="${clientRB}"/>
                </span>
            </div>
            </div>
        </c:otherwise>
    </c:choose>
    <ctg:pageTag pageSize="${pageSize}"/>

    <div class="modal-form" id="modalClient">
        <span class="modal_close">X</span>
        <div class="form-item">
            <select id="select">
                <c:forEach var="elem" items="${listBonus}">
                    <option value=${elem.id}>${elem.name} ${elem.size} ${elem.units}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-item">
            <input type='submit' value='<fmt:message key="button.save" bundle="${mainRB}"/>' class="default-button">
            <input type='reset' value='<fmt:message key="button.clear" bundle="${mainRB}"/>' class="default-button">
        </div>
    </div>
    <div class="modal-form" id="modalAdd">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="name" class="form-item-label">
                <fmt:message key="label.name" bundle="${bonusRB}"/>
            </label>
            <input id="name" type="hidden" class="form-input" name="name">
            <select id="selectName">
                <option><fmt:message key="label.freeaudio" bundle="${bonusRB}"/></option>
                <option><fmt:message key="label.discount" bundle="${bonusRB}"/></option>
            </select>
        </div>
        <div class="form-item">
            <label for="size" class="form-item-label">
                <fmt:message key="label.quantity" bundle="${bonusRB}"/>
            </label>
            <input id="size" class="form-input" name="size">
            <span name="errorSize" class="error-message">
                <fmt:message key="error.bonus.size" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="units" class="form-item-label">
                <fmt:message key="label.units" bundle="${bonusRB}"/>
            </label>
            <input id="units" class="form-input" name="units">
            <span name="errorUnits" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <input type='submit' value='<fmt:message key="button.save" bundle="${mainRB}"/>' class="default-button">
            <input type='reset' value='<fmt:message key="button.clear" bundle="${mainRB}"/>' class="default-button">
        </div>
    </div>
    <div id="overlay"></div>
</form>
<footer class="footer"><fmt:message key="footer" bundle="${mainRB}"/></footer>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/default.js"></script>
<script src="/js/validation/validationBonus.js"></script>
<script src="/js/mainAdmin.js"></script>
</body>
</html>
