/**
 * Created by Босс on 10.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]'),
    audioId = $('[name = audioId]'),
    usingBonus = false,
    totalCost = $('[name = totalCost]');

setTotalCost();

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Pay':
        case 'Opłacić':
        case 'Оплатить':
            command.val(usingBonus == true ? "updateBonus" : "update");
            commandObj.val("order");
            break;
        case 'Delete':
        case 'Usunąć':
        case 'Удалить':
            var id = event.target.parentElement.getAttribute("id");
            command.val("delete");
            commandObj.val("order");
            audioId.val(id);
            break;
        case 'Бонус':
            var bonusName = $('#bonusCheck').attr('name'),
                bonusSize = +($('#bonusCheck').attr('size'));
            if ($('#bonusCheck').is(':checked')) {
                useBonus(bonusName, bonusSize);
            }
            else {
                setTotalCost();
            }
            break;
    }
});

$('.info-img').on('click', function (event) {
    var hrefAudio = event.target.parentElement,
        id = hrefAudio.parentElement.parentElement.getAttribute("id");
    hrefAudio.href = '/audio_store?commandObject=audioTrack&command=get&audioId='+id;
});

function useBonus(bonusName, bonusSize) {
    switch (bonusName) {
        case 'Discount':
        case 'Zniżka':
        case 'Скидка':
            var cost = totalCost.text(),
                total = +(cost.substr(0, cost.length - 2)) * (100 - bonusSize) / 100;
            totalCost.text(total.toFixed(2) + ' $');
            break;
        case 'Free audio track':
        case 'Bezpłatna ścieżka audio':
        case 'Бесплатный аудио трек':
            setFreeAudio(bonusSize);
            break;
    }
    usingBonus = true;
}

function setFreeAudio(bonusSize) {
    var prices = $('[name = price]'),
        pricesMas = [prices.length],
        total = 0;
    prices.each(function (indx, element) {
        var elementVal = $(element).text();
        pricesMas[indx] = +(elementVal.substr(0, elementVal.length - 2));
    });
    prices.each(function (indx, element) {
        if (bonusSize != 0) {
            var elementVal = $(element).text(),
                elementPrice = +(elementVal.substr(0, elementVal.length - 2)),
                count = 0;
            for (var i = 0; i < pricesMas.length; i++) {
                if (elementPrice <= pricesMas[i]) {
                    count++;
                }
            }
            if (count >= pricesMas.length - bonusSize + 1) {
                $(element).after(function () {
                    return '<span name="newPrice">0.0 $</span>';
                });
                $(element).css('display', 'none');
            }
            else{
                total += elementPrice;
            }
        }
    });
    totalCost.text(total.toFixed(2) + " $");
}

function setTotalCost() {
    var total = 0;
    if(!$('#bonusCheck').attr('name')){
        $('.bonus-block').remove();
    }
    if(!$('.audio-block').attr('id')){
        $('.total-cost').css('display', 'none');
        $('.bonus-block').remove();
    }
    $('[name = price]').each(function (indx, element) {
        var element = $(element).text();
        total += +(element.substr(0, element.length - 2));
        totalCost.text(total.toFixed(2) + " $");
    });
    $('[name = newPrice]').each(function (indx, element) {
        $(element).css('display', 'none');
    });
    $('[name = price]').each(function (indx, element) {
        $(element).css('display', 'inline-block');
    });
    usingBonus = false;
}

