
/**
 * Created by Босс on 10.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]'),
    audioId = $('[name = audioId]');

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case'Add to the basket':
        case 'Dodać do koszyka':
        case 'Добавить в корзину':
            var id = event.target.parentElement.getAttribute("id");
            command.val("addRecommends");
            commandObj.val("order");
            audioId.val(id);
            break;
        case 'Search':
        case 'Poszukiwanie':
        case 'Поиск':
            var audioFind = $('[name=audioFind]').val();
            location.href='/audio_store?commandObject=audioTrack&command=recommends&audioFind=' + audioFind;
            //command.val("recommends");
            //commandObj.val("audiotrack");
            break;

    }
});

$('.page-button').on('click', function (event) {
    var number = event.target.value;
    var audioFind = $('[name=audioFind]').val();
    location.href='/audio_store?commandObject=audioTrack&command=recommends&audioFind=' + audioFind
        + "&pageButton=" + number;

});

$('.info-img').on('click', function (event) {
    var hrefAudio = event.target.parentElement,
        id = hrefAudio.parentElement.parentElement.getAttribute("id");
    hrefAudio.href = '/audio_store?commandObject=audioTrack&command=get&audioId='+id;
});



