/**
 * Created by Босс on 10.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]'),
    audioId = $('[name = audioId]');

switch (getCookie('clientType')) {
    case 'ADMIN':
        $('[name=addAudio]').css({display: "inline-block"});
        $('[name = deleteAudio]').css({display: "inline-block"});
        $('.header :last-child').prev().prev().remove();
        $('.header :last-child').prev().prev().remove();
        break;
    case 'USER':
        $('[name = addToBasket]').css({display: "inline-block"});
        break;
}

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Add to the basket':
        case 'Dodać do koszyka':
        case 'Добавить в корзину':
            var id = event.target.parentElement.getAttribute("id");
            command.val("add");
            commandObj.val("order");
            audioId.val(id);
            break;
        case 'Delete':
        case 'Usunąć':
        case 'Удалить':
            var id = event.target.parentElement.getAttribute("id");
            command.val("delete");
            commandObj.val("audiotrack");
            audioId.val(id);
            break;
        case 'Add new audio track':
        case 'Dodać nową ścieżkę audio':
        case 'Добавить новый аудио трек':
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalAudio')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Search':
        case 'Poszukiwanie':
        case 'Поиск':
            var audioFind = $('[name=audioFind]').val();
            location.href='/audio_store?commandObject=audioTrack&command=find&audioFind=' + audioFind;
            break;

    }
});

$('.page-button').on('click', function (event) {
    var number = event.target.value;
    var audioFind = $('[name=audioFind]').val();
    location.href='/audio_store?commandObject=audioTrack&command=find&audioFind=' + audioFind
        + "&pageButton=" + number;

});



$('.info-img').on('click', function (event) {
    var hrefAudio = event.target.parentElement,
        id = hrefAudio.parentElement.parentElement.getAttribute("id");
    hrefAudio.href = '/audio_store?commandObject=audioTrack&command=get&audioId=' + id;
});

$('#modalAudio').on('click', function (event) {
    switch (event.target.value) {
        case 'Save':
        case 'Zachować':
        case 'Сохранить':
            if (validateAudio()) {
                command.val("add");
                commandObj.val("audiotrack");
                return true;
            }
            return false;
    }
});

