/**
 * Created by Босс on 24.04.2017.
 */

/**
 * Created by Босс on 03.04.2017.
 */
function validateBonus(){
    var size = document.getElementById('size'),
        units = document.getElementById('units'),
        check = true;
    if(!checkSize(size)){
        check = false;
    }
    if(!checkUnits(units)){
        check = false;
    }
    return check;
}

function checkSize(size){
    var error = document.getElementsByName('errorSize')[0],
        regExp = /^[0-9]{1,2}$/,
        sizeValue = size.value;
    error.style.display = 'none';
    if(!toggleInput(size)){
        return false;
    }
    if(!regExp.test(sizeValue)){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkUnits(units){
    var error = document.getElementsByName('errorUnits')[0],
        regExp = /^[а-яa-z%]+$/,
        unitsValue = units.value;
    error.style.display = 'none';
    if(!toggleInput(units)){
        return false;
    }
    if(!regExp.test(unitsValue)){
        error.append(': а-я, a-z, %');
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}


function toggleInput(element) {
    if (!element.value) {
        element.style.boxShadow = '0px 0px 5px 5px rgba(255, 178, 178, 1)';
        return false;
    }
    element.style.boxShadow = 'none';
    return true;
}






