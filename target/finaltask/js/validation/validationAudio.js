/**
 * Created by Босс on 10.04.2017.
 */
function validateAudio(){
    var author = document.getElementById('author'),
        name = document.getElementById('name'),
        year = document.getElementById('year'),
        genre = document.getElementById('genre'),
        price =  document.getElementById('price'),
        site = document.getElementById('site'),
        check = true;
     if(!checkAuthor(author)){
        check = false;
    }
    if(!checkName(name)){
        check = false;
    }
    if(!checkYear(year)){
        check = false;
    }
    if(!checkGenre(genre)){
        check = false;
    }
    if(!checkPrice(price)){
        check = false;
    }
    if(site != null && !toggleInput(site)){
        check = false;
    }
    return check;
}
function checkAuthor(author){
    var error = document.getElementsByName('errorAuthor')[0],
        regExp = /^[\wа-яА-Я\s&-]+$/i,
        authorValue = author.value;
    error.style.display = 'none';
    if(!toggleInput(author)){
        return false;
    }
    if(!regExp.test(authorValue)){
        error.append(': A-z, А-я, 0-9, _');
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkName(name){
    var error = document.getElementsByName('errorName')[0],
        regExp = /^[\wа-яА-Я\s-]+$/i,
        nameValue = name.value;
    error.style.display = 'none';
    if(!toggleInput(name)){
        return false;
    }
    if(!regExp.test(nameValue)){
        error.append(': A-z, А-я, 0-9, _');
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkYear(year){
    var error = document.getElementsByName('errorYear')[0],
        regExp = /^[1-2]\d{3}$/,
        yearValue = year.value;
    error.style.display = 'none';
    if(!toggleInput(year)){
        return false;
    }
    if(!regExp.test(yearValue)){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function checkGenre(genre){
    var error = document.getElementsByName('errorGenre')[0],
        regExp = /^[a-zа-яА-ЯA-Z-\\s]+$/i,
        genreValue = genre.value;
    error.style.display = 'none';
    if(!toggleInput(genre)){
        return false;
    }
    if(!regExp.test(genreValue)){
        error.style.display = 'inline-block';
        error.append(': A-z, А-я,-');
        return false;
    }
    return true;
}

function checkPrice(price){
    var error = document.getElementsByName('errorPrice')[0],
        regExp = /^[\d]+\.?[\d]*$/,
        priceValue = price.value;
    error.style.display = 'none';
    if(!toggleInput(price)){
        return false;
    }
    if(!regExp.test(priceValue)){
        error.style.display = 'inline-block';
        return false;
    }
    return true;
}

function toggleInput(element) {
    if (!element.value) {
        element.style.boxShadow = '0px 0px 5px 5px rgba(255, 178, 178, 1)';
        return false;
    }
    element.style.boxShadow = 'none';
    return true;
}