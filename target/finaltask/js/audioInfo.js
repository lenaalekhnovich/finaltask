
/**
 * Created by Босс on 10.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]');

switch(getCookie('clientType')){
    case 'ADMIN':
        $('.header :last-child').prev().prev().remove();
        $('.header :last-child').prev().prev().remove();
        $('#updateAudio').css({ display: "inline-block" });
        break;
    case 'USER':
        $('.review').css({ display: "inline-block" });
        break;
}

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Leave a review':
        case 'Zostawić opinia':
        case 'Оставить отзыв':
            if(validateContent()) {
                var id = event.target.parentElement.getAttribute("id");
                command.val("add");
                commandObj.val("review");
                return true;
            }
            return false;
        case 'Update audio track':
        case 'Edytować ścieżkę audio':
        case 'Редактировать аудио трек':
            $('#name').val($('#spanName').html());
            $('#author').val($('#spanAuthor').html());
            $('#genre').val($('#spanGenre').html());
            $('#year').val($('#spanYear').html());
            $('#price').val($('#spanPrice').html());
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalAudio')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
    }
});

$('.header a').on('click', function (event) {
    switch (event.target.text()) {
        case 'Exit':
        case 'Wyjście':
        case 'Выход':
            var cookie_date = new Date();
            cookie_date.setTime(cookie_date.getTime() - 1);
            document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
            document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
            break;
    }
});

$('#modalAudio').on('click', function (event) {
    switch (event.target.value) {
        case 'Save':
        case 'Zachować':
        case 'Сохранить':
            if (validateAudio()) {
                command.val("update");
                commandObj.val("audiotrack");
                return true;
            }
            return false;
    }
});

function validateContent(){
    var content = $('[name = reviewText]'),
        error = $('[name = errorReview]'),
        errorLength = $('[name = errorReviewLength]'),
        regExp = /^[-\w\s?,;.:!()а-яА-я]{10,}$/i,
        contentValue = content.val();
    error.css('display', 'none');
    errorLength.css('display', 'none');
    content.css('boxShadow', 'none');
    if (!contentValue) {
        content.css('boxShadow', '0px 0px 5px 5px rgba(255, 178, 178, 1)');
        return false;
    }
    if(contentValue.length < 10){
        errorLength.append(' 10');
        errorLength.css('display', 'inline-block');
        return false;
    }
    if(!regExp.test(contentValue)){
        error.append(': А-я, A-z,:;.!?');
        error.css('display', 'inline-block');
        return false;
    }
    return true;
}


