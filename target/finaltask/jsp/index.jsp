<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.client" var="clientRB"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/auth.css">
    <title><fmt:message key="label.auth" bundle="${mainRB}"/></title>
</head>
<body>
<ctg:localeTag/>
<div class="wrapper">
    <c:set var="error" value="${error}"/>
    <c:if test="${not empty error}">
        <div id="error" class="error-message">${error}</div>
    </c:if>
    <form class="form" method="POST" action="audio_store">
        <input type="hidden" name="commandObject" value="client">
        <input type="hidden" name="command" value="login">
        <div class="form-item">
            <label for="login" class="form-item-label">
                <fmt:message key="label.login" bundle="${clientRB}"/>
            </label>
            <input id="login" class="form-input" name="login" value="${login}">
        </div>
        <div class="form-item">
            <label for="password" class="form-item-label">
                <fmt:message key="label.password" bundle="${clientRB}"/>
            </label>
            <input id="password" class="form-input" type="password" name="password">
        </div>
        <div class="form-item">
            <input type="checkbox" name="role" value="">
            <fmt:message key="label.signInAdmin" bundle="${mainRB}"/>
        </div>
        <div class="form-item">
            <input class="button-form" type="submit" value="<fmt:message key="button.signIn" bundle="${mainRB}"/>">
            <input class="button-form" type="button" value="<fmt:message key="button.signUp" bundle="${mainRB}"/>">
            <input class="button-form" type="reset" value="<fmt:message key="button.clear" bundle="${mainRB}"/>">
        </div>
    </form>
</div>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/authorization.js"></script>
</body>
</html>