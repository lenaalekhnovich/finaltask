<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 05.03.2017
  Time: 2:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<fmt:requestEncoding value="utf-8"/>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.client" var="clientRB"/>
<fmt:setBundle basename="pagecontent.audio" var="audioRB"/>
<fmt:setBundle basename="pagecontent.bonus" var="bonusRB"/>
<fmt:setBundle basename="pagecontent.error" var="errorRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/mainUser.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <link rel="stylesheet" type="text/css" href="/css/modalForm.css">
    <title><fmt:message key="header.main" bundle="${mainRB}"/></title>
</head>
<body>
<header class="header" method="GET"><fmt:message key="header.main" bundle="${mainRB}"/>
    <a href="/audio_store?commandObject=audioTrack&command=find">
        <fmt:message key="header.audio" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=order&command=find">
        <fmt:message key="header.basket" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=audioTrack&command=recommends">
        <fmt:message key="header.recommends" bundle="${mainRB}"/>
    </a>
    <a onclick='exit()' href='/audio_store'>
        <fmt:message key='label.exit' bundle='${mainRB}'/>
    </a>
    <ctg:localeTag/>
</header>
<form class="mainForm" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="">
    <input type="hidden" name="command" value="">
    <div class="button-group">
        <input type="button" value="<fmt:message key="button.updateInfo" bundle="${clientRB}"/>">
        <input type="button" value="<fmt:message key="button.updatePas" bundle="${clientRB}"/>">
        <input type="submit" value="<fmt:message key="label.exit" bundle="${mainRB}"/>">
    </div>
    <div class="info-about">
        <div class="tittle">
            <p><fmt:message key="label.info" bundle="${clientRB}"/></p>
        </div>
        <p><fmt:message key="label.name" bundle="${clientRB}"/></p>
        <span id="spanName">${client.name}</span>
        <p><fmt:message key="label.login" bundle="${clientRB}"/></p>
        <span id="spanLogin">${client.login}</span>
        <p><fmt:message key="label.date" bundle="${clientRB}"/></p>
        <span id="spanDate">${client.dateBirth}</span>
        <p><fmt:message key="label.email" bundle="${clientRB}"/></p>
        <span id="spanEmail">${client.email}</span>
        <p><fmt:message key="label.credit" bundle="${clientRB}"/></p>
        <span id="spanCredit">${client.creditCard}</span>
        <p><fmt:message key="label.bonus" bundle="${clientRB}"/></p>
        <c:set var="bonus" value="${client.bonus}"/>
        <c:choose>
            <c:when test="${empty bonus}">
                <span><fmt:message key="label.notFound" bundle="${bonusRB}"/></span>
            </c:when>
            <c:otherwise>
                <span>${client.bonus.name} (${client.bonus.size} ${client.bonus.units})</span>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="modal-form" id="modalPassword">
        <span class="modal_close">X</span>
        <div class="form-item">
            <input type="hidden" name="trueOldPassword" value="${client.password}"/>
            <label for="oldPassword" class="form-item-label">
                <fmt:message key="label.curPassword" bundle="${clientRB}"/>
            </label>
            <input type='password' id='oldPassword' class="form-input">
            <span name="errorOldPassword" class="error-message">
                <fmt:message key="error.client.oldPas" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="password" class="form-item-label">
                <fmt:message key="label.newPassword" bundle="${clientRB}"/>
            </label>
            <input id="password" name="password" class="form-input" type="password">
            <span name="errorPasswordLength" class="error-message">
                <fmt:message key="error.length" bundle="${errorRB}"/>
            </span>
            <span name="errorPassword" class="error-message">
                <fmt:message key="error.client.password" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="passwordRepeat" class="form-item-label">
                <fmt:message key="label.conPassword" bundle="${clientRB}"/>
            </label>
            <input id="passwordRepeat" class="form-input" type="password">
            <span name="errorRepeatPassword" class="error-message">
                <fmt:message key="error.client.repeatPas" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <input type='submit' value='<fmt:message key="button.save" bundle="${mainRB}"/>'>
            <input type='reset' value='<fmt:message key="button.clear" bundle="${mainRB}"/>'>
        </div>
    </div>
    <div class="modal-form" id="modalClient">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="name" class="form-item-label">
                <fmt:message key="label.name" bundle="${clientRB}"/>
            </label>
            <input id="name" class="form-input" name="name">
            <span name="errorNameFirst" class="error-message">
                <fmt:message key="error.client.name.firstLetter" bundle="${errorRB}"/>
            </span>
            <span name="errorName" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="login" class="form-item-label">
                <fmt:message key="label.login" bundle="${clientRB}"/>
            </label>
            <input id="login" class="form-input" name="login">
            <span name="errorLoginLength" class="error-message">
                <fmt:message key="error.length" bundle="${errorRB}"/>
            </span>
            <span name="errorLogin" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="date" class="form-item-label">
                <fmt:message key="label.date" bundle="${clientRB}"/>
            </label>
            <input type="date" id="date" class="form-input" name="dateBirth">
            <span name="errorDate" class="error-message">
                <fmt:message key="error.client.year" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="email" class="form-item-label">
                <fmt:message key="label.email" bundle="${clientRB}"/>
            </label>
            <input id="email" class="form-input" name="email">
            <span name="errorEmail" class="error-message">
                <fmt:message key="error.client.email" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="creditCard" class="form-item-label">
                <fmt:message key="label.credit" bundle="${clientRB}"/>
            </label>
            <input id="creditCard" class="form-input" name="creditCard">
            <span name="errorCredit" class="error-message">
                <fmt:message key="error.client.credit" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <input type='submit' value='<fmt:message key="button.save" bundle="${mainRB}"/>'>
            <input type='reset' value='<fmt:message key="button.clear" bundle="${mainRB}"/>'>
        </div>
    </div>
    <div id="overlay"></div>
</form>
<div class="info-audiotrack">
    <div class="tittle">
        <p><fmt:message key="label.myAudio" bundle="${audioRB}"/></p>
    </div>
    <c:set var="orders" value="${client.orderList}"/>
    <c:choose>
        <c:when test="${not empty orders}">
            <ul>
                <c:forEach var="elem" items="${client.orderList}">
                    <li>
                        <div class="info-audio-name">
                            <p>${elem.audioTrack.author} - ${elem.audioTrack.name}</p>
                        </div>
                        <div class="info-img" id="${elem.audioTrack.id}">
                            <a href="">
                                <img class="info-img" src="/resource/images/information-icon.png">
                            </a>
                        </div>
                        <a href="${elem.audioTrack.site}" download>
                            <fmt:message key="label.download" bundle="${audioRB}"/>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <p>
                <fmt:message key="label.audio.notBuy" bundle="${audioRB}"/>
            </p>
        </c:otherwise>
    </c:choose>
</div>
<footer class="footer"><fmt:message key="footer" bundle="${mainRB}"/></footer>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/resource/MD5.js"></script>
<script src="/js/default.js"></script>
<script src="/js/validation/validationClient.js"></script>
<script src="/js/mainUser.js"></script>
</body>
</html>
