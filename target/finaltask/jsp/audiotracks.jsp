<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 10.04.2017
  Time: 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.audio" var="audioRB"/>
<fmt:setBundle basename="pagecontent.order" var="orderRB"/>
<fmt:setBundle basename="pagecontent.error" var="errorRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <link rel="stylesheet" type="text/css" href="/css/audiotracks.css">
    <link rel="stylesheet" type="text/css" href="/css/modalForm.css">
    <title><fmt:message key="header.audio" bundle="${mainRB}"/></title>
</head>
<body>
<header class="header">
    <a href="/audio_store">
        <fmt:message key="header.main" bundle="${mainRB}"/>
    </a>
    <fmt:message key="header.audio" bundle="${mainRB}"/>
    <a href="/audio_store?commandObject=order&command=find">
        <fmt:message key="header.basket" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=audioTrack&command=recommends">
        <fmt:message key="header.recommends" bundle="${mainRB}"/>
    </a>
    <a onclick='exit()' href='/audio_store'>
        <fmt:message key='label.exit' bundle='${mainRB}'/>
    </a>
    <ctg:localeTag/>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="audioTrack">
    <input type="hidden" name="command" value="find">
    <input type="hidden" name="audioId" value="">
    <input type="hidden" name="page" value="${page}">
    <div class="audio-block">
        <input type="text" name="audioFind" value="${audioFind}">
        <input type="button" value="<fmt:message key="button.find" bundle="${mainRB}"/>" class="default-button">
        <input type="submit" name="addAudio" value="<fmt:message key="button.add" bundle="${audioRB}"/>"
               class="default-button add-button">
    </div>
    <c:set var="audioList" value="${audiotracks}"/>
    <c:choose>
        <c:when test="${not empty audioList}">
            <c:forEach var="elem" items="${audiotracks}" begin="${(page-1)*6}" end="${page*6-1}">
                <ctg:audioTag id="${elem.id}" name="${elem.name}" author="${elem.author}" price="${elem.price}"/>
                <input type="submit" name="addToBasket"
                       value="<fmt:message key="button.order.add" bundle="${orderRB}"/>"
                       class="default-button none-display">
                <input type="submit" name="deleteAudio" value="<fmt:message key="button.delete" bundle="${mainRB}"/>"
                       class="default-button none-display"></div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="audio-block">
                <span><fmt:message key="label.audio.notFind" bundle="${audioRB}"/></span>
            </div>
        </c:otherwise>
    </c:choose>
    <ctg:pageTag pageSize="${pageSize}"/>
    <div class="modal-form" id="modalAudio">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="author" class="form-item-label">
                <fmt:message key="label.author" bundle="${audioRB}"/>
            </label>
            <input id="author" class="form-input" name="author">
            <span name="errorAuthor" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="name" class="form-item-label">
                <fmt:message key="label.name" bundle="${audioRB}"/>
            </label>
            <input id="name" class="form-input" name="name">
            <span name="errorName" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="year" class="form-item-label">
                <fmt:message key="label.year" bundle="${audioRB}"/>
            </label>
            <input id="year" class="form-input" name="year">
            <span name="errorYear" class="error-message">
                 <fmt:message key="error.audio.year" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="genre" class="form-item-label">
                <fmt:message key="label.genre" bundle="${audioRB}"/>
            </label>
            <input id="genre" class="form-input" name="genre">
            <span name="errorGenre" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="price" class="form-item-label">
                <fmt:message key="label.price" bundle="${audioRB}"/>
            </label>
            <input id="price" class="form-input" name="price">
            <span name="errorPrice" class="error-message">
                <fmt:message key="error.audio.price" bundle="${errorRB}"/>
            </span>
        </div>
        <div class="form-item">
            <label for="site" class="form-item-label">
                <fmt:message key="label.site" bundle="${audioRB}"/>
            </label>
            <input id="site" class="form-input" name="site">
        </div>
        <div class="form-item">
            <input type='submit' value='<fmt:message key="button.save" bundle="${mainRB}"/>' class="default-button">
            <input type='reset' value='<fmt:message key="button.clear" bundle="${mainRB}"/>' class="default-button">
        </div>
    </div>
    <div id="overlay"></div>
</form>
<footer class="footer"><fmt:message key="footer" bundle="${mainRB}"/></footer>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/default.js"></script>
<script src="/js/validation/validationAudio.js"></script>
<script src="/js/audiotracks.js"></script>
</body>
</html>
