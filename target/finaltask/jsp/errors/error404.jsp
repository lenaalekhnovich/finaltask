<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.error" var="errorRB"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <link rel="stylesheet" type="text/css" href="/css/error.css">
    <title><fmt:message key="error.name" bundle="${errorRB}"/></title>
</head>
<body>
<form class="audio-form">
    <div class="error">
        <p><fmt:message key="error.page.404" bundle="${errorRB}"/></p>
        <a href="/audio_store">
            <fmt:message key="error.page.main" bundle="${errorRB}"/>
        </a>
    </div>
</form>
<footer class="footer">
    <fmt:message key="footer" bundle="${mainRB}"/>
</footer>
</body>
</html>