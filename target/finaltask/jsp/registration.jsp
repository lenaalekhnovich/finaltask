<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 12.04.2017
  Time: 0:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.client" var="clientRB"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.error" var="errorRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/registration.css">
    <title><fmt:message key="button.signUp" bundle="${mainRB}"/></title>
</head>
<body>
<header class="header">
</header>
<form class="form" method="POST" name="registrForm" action="audio_store">
    <input type="hidden" name="commandObject" value="client">
    <input type="hidden" name="command" value="registration">
    <div class="form-item">
        <label for="name" class="form-item-label">
            <fmt:message key="label.name" bundle="${clientRB}"/>
        </label>
        <input id="name" class="form-input" name="name">
        <span name="errorNameFirst" class="error-message">
                <fmt:message key="error.client.name.firstLetter" bundle="${errorRB}"/>
        </span>
        <span name="errorName" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <label for="login" class="form-item-label">
            <fmt:message key="label.login" bundle="${clientRB}"/>
        </label>
        <input id="login" class="form-input" name="login">
        <span name="errorLoginLength" class="error-message">
                <fmt:message key="error.length" bundle="${errorRB}"/>
        </span>
        <span name="errorLogin" class="error-message">
                <fmt:message key="error.symbol" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <label for="date" class="form-item-label">
            <fmt:message key="label.date" bundle="${clientRB}"/>
        </label>
        <input type="date" id="date" class="form-input" name="dateBirth">
        <span name="errorDate" class="error-message">
                <fmt:message key="error.client.year" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <label for="email" class="form-item-label">
            <fmt:message key="label.email" bundle="${clientRB}"/>
        </label>
        <input id="email" class="form-input" name="email">
        <span name="errorEmail" class="error-message">
                <fmt:message key="error.client.email" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <label for="creditCard" class="form-item-label">
            <fmt:message key="label.credit" bundle="${clientRB}"/>
        </label>
        <input id="creditCard" class="form-input" name="creditCard">
        <span name="errorCredit" class="error-message">
                <fmt:message key="error.client.credit" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <label for="password" class="form-item-label">
            <fmt:message key="label.password" bundle="${clientRB}"/>
        </label>
        <input id="password" class="form-input" type="password" name="password">
        <span name="errorPasswordLength" class="error-message">
                <fmt:message key="error.length" bundle="${errorRB}"/>
        </span>
        <span name="errorPassword" class="error-message">
                <fmt:message key="error.client.password" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <label for="passwordRepeat" class="form-item-label">
            <fmt:message key="label.conPassword" bundle="${clientRB}"/>
        </label>
        <input id="passwordRepeat" class="form-input" type="password" name="passwordRepeat">
        <span name="errorRepeatPassword" class="error-message">
                <fmt:message key="error.client.repeatPas" bundle="${errorRB}"/>
        </span>
    </div>
    <div class="form-item">
        <input class="button-style" type="submit" value="<fmt:message key="button.signUp" bundle="${mainRB}"/>">
        <input class="button-style" type="reset" value="<fmt:message key="button.clear" bundle="${mainRB}"/>">
    </div>
    <div class="form-item">
        <input class="button-style" type="button" value="<fmt:message key="button.signIn" bundle="${mainRB}"/>">
    </div>
</form>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/resource/MD5.js"></script>
<script src="/js/validation/validationClient.js"></script>
<script src="/js/registration.js"></script>
</body>
</html>
