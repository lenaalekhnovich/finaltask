<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 10.04.2017
  Time: 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="audiotags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent.page" var="mainRB"/>
<fmt:setBundle basename="pagecontent.audio" var="audioRB"/>
<fmt:setBundle basename="pagecontent.order" var="orderRB"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <link rel="stylesheet" type="text/css" href="/css/audiotracks.css">
    <title><fmt:message key="header.recommends" bundle="${mainRB}"/></title>
</head>
<body>
<header class="header" method="GET">
    <a href="/audio_store">
        <fmt:message key="header.main" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=audioTrack&command=find">
        <fmt:message key="header.audio" bundle="${mainRB}"/>
    </a>
    <a href="/audio_store?commandObject=order&command=find">
        <fmt:message key="header.basket" bundle="${mainRB}"/>
    </a>
    <fmt:message key="header.recommends" bundle="${mainRB}"/>
    <a onclick='exit()' href='/audio_store'>
        <fmt:message key='label.exit' bundle='${mainRB}'/>
    </a>
    <ctg:localeTag/>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="audioTrack">
    <input type="hidden" name="command" value="recommends">
    <input type="hidden" name="audioId" value="">
    <div class="audio-block">
        <input type="text" name="audioFind" value="${audioFind}">
        <input type="button" value="<fmt:message key="button.find" bundle="${mainRB}"/>" class="default-button">
    </div>
    <c:set var="audioList" value="${audiotracks}"/>
    <c:choose>
        <c:when test="${not empty audioList}">
            <c:forEach var="elem" items="${audiotracks}" begin="${(page-1)*6}" end="${page*6-1}">
                <ctg:audioTag id="${elem.id}" name="${elem.name}" author="${elem.author}" price="${elem.price}"/>
                <input type='submit' name='addToBasket' value="<fmt:message key="button.order.add" bundle="${orderRB}"/>"
                       class='default-button'>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="audio-block">
                <span>
                    <fmt:message key="label.recommends.notFind" bundle="${audioRB}"/>
                </span>
            </div>
        </c:otherwise>
    </c:choose>
    <ctg:pageTag pageSize="${pageSize}"/>
</form>
<footer class="footer"><fmt:message key="footer" bundle="${mainRB}"/></footer>
<script src="/js/resource/jquery-3.1.1.min.js"></script>
<script src="/js/default.js"></script>
<script src="/js/recommends.js"></script>
</body>
</html>
